//
//  NFCManager.m
//  NFCManager
//
//  Created by Emmanuel Castellani on 03/10/12.
//  Copyright (c) 2012 Emmanuel Castellani. All rights reserved.
//

#import "NFCManager.h"
#import "iCarteSDK.h"
#import <objc/objc-sync.h>
#import <AudioToolbox/AudioServices.h>

#define kSCAN_INTERVAL 0.7
#define kSCAN_PAUSE 3


#define AID_LENGTH 15
#define AID_0 0x63 // c
#define AID_1 0x6F // o
#define AID_2 0x6D // m
#define AID_3 0x2E // .
#define AID_4 0x74 // t
#define AID_5 0x6E // n
#define AID_6 0x67 // g
#define AID_7 0x2E // .
#define AID_8 0x63 // c
#define AID_9 0x61 // a
#define SE_CLA 0x00
#define SE_INS 0xA4
#define SE_P1 0x04
#define SE_P2 0x00
#define SE_LC 0x0A

#define MAX_NUMBER_OF_PROD_FOR_APDU_1 21
#define MAX_NUMBER_OF_PROD_FOR_OTHER_APDU 23

#define APDU_WRITE_PRODUCT_LENGTH 16
#define APDU_WRITE_PRODUCT_0 0x00
#define APDU_WRITE_PRODUCT_1 0xD1
#define APDU_WRITE_PRODUCT_P1 0x00  // not used
#define APDU_WRITE_PRODUCT_P2 1     // index of start from product to write. To be set
#define APDU_WRITE_PRODUCT_LG 0x0B  // Length of the rest of the apdu command. Here 7 for ean + 1 for qty + 3 for price

#define APDU_WRITE_PRODUCTS_MIN_LENGTH 5
#define APDU_WRITE_PRODUCTS_QUANTITY_NUMBER_LENGTH 1
#define APDU_WRITE_PRODUCTS_ID_CUSTOMER_ID_LENGTH 1
#define APDU_WRITE_PRODUCTS_CUSTOMER_ID_LENGTH 10
#define APDU_WRITE_PRODUCTS_PRODUCT_INFO_LENGTH 11  // 7 for EAN + 1 for prod quantity + 3 for prod price
#define APDU_WRITE_PRODUCTS_0 0x00
#define APDU_WRITE_PRODUCTS_1 0xDA
#define APDU2_WRITE_PRODUCTS_1 0xDB
#define APDU3_WRITE_PRODUCTS_1 0xDC
#define APDU4_WRITE_PRODUCTS_1 0xDD
#define APDU5_WRITE_PRODUCTS_1 0xDE
#define APDU6_WRITE_PRODUCTS_1 0xDF
#define APDU_WRITE_PRODUCTS_P1 0x00 // Finaly not used    // total numbers of ean. To be set
#define APDU_WRITE_PRODUCTS_P2 0x01 // if P1 > 22 products then P2 is number of products left in next apdu
#define APDU_WRITE_PRODUCTS_LG (APDU_WRITE_PRODUCTS_QUANTITY_NUMBER_LENGTH + APDU_WRITE_PRODUCTS_ID_CUSTOMER_ID_LENGTH + APDU_WRITE_PRODUCTS_CUSTOMER_ID_LENGTH + (APDU_WRITE_PRODUCTS_PRODUCT_INFO_LENGTH * APDU_WRITE_PRODUCTS_P1)) // Length of the rest of the apdu command. To be set
#define APDU_WRITE_PRODUCTS_ID_CUSTOMER_ID 0xCA

NSString *const NOTIFICATION_NFC_DETECTED_FOUND_TAG     = @"NFCDetectedFoundTag";
NSString *const NOTIFICATION_NFC_DETECTED_UNKNOWN_TAG     = @"NFCDetectedUnknownTag";
NSString *const NOTIFICATION_NO_NFC_DEVICE            = @"NoNFCDeviceDetected";
NSString *const NOTIFICATION_NFC_DEVICE_CONNECTED     = @"NFCDeviceGotConnected";
NSString *const NOTIFICATION_NFC_DEVICE_DISCONNECTED  = @"NFCDeviceGotDisconnected";

NSString *const KEY_SYNC_ID      = @"SyncID";
NSString *const KEY_TAG_ID       = @"TagID";


@interface NFCManager()
{
    /**
     The timer used to execute the NFC Read Action.
     */
    dispatch_source_t nfcTimer;
    /**
     The error manager.
     */
    ERR errorManager;
    /**
     The NFC Device manager.
     */
    Utilities *utilities;
    /**
     The MifareULC Tag manager.
     */
    ICODESLI *tagger_ICODESLI;
    /**
     The MifareULC Tag manager.
     */
    Kovio *tagger_Kovio;
    /**
     The Mifare1K Tag manager.
     */
    Mifare1K *tagger_Mifare1K;
    /**
     The Mifare4K Tag manager.
     */
    Mifare4K *tagger_Mifare4K;
    /**
     The MifareUL Tag manager.
     */
    MifareUL *tagger_MifareUL;
    /**
     The MifareULC Tag manager.
     */
    MifareULC *tagger_MifareULC;
    /**
     The MifareUL Tag manager.
     */
    Topaz *tagger_Topaz;
    /**
     Flag
     */
    BOOL timerIsStopped;
}
- (void) initNFC;
- (void) pauseTimer;
- (void) relaunchTimer;

- (void) cleanTaggers;
- (void) initializeTaggers;
@end

@implementation NFCManager
@synthesize SCAN_INTERVAL;
@synthesize SCAN_PAUSE;
@synthesize SCAN_TAG_TYPE;
@synthesize SCAN_TAG_MASK;


+ (NFCManager*)sharedNFCManager
{
	static  NFCManager *instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
    {
		instance = [[NFCManager alloc] init];
        
	});
	return instance;
}

- (id)init
{
    self = [super init];
    if(self)
    {
        [self initNFC];
    }
    return self;
}

- (void) initNFC
{
    //inialize with default values
    self.SCAN_INTERVAL = kSCAN_INTERVAL;
    self.SCAN_PAUSE = kSCAN_PAUSE;
    
    //set all to nil
    tagger_ICODESLI = nil;
    tagger_Kovio = nil;
    tagger_Mifare1K = nil;
    tagger_Mifare4K = nil;
    tagger_MifareUL = nil;
    tagger_MifareULC = nil;
    tagger_Topaz = nil;
    
    // add notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCarteConnected:) name:iCarteConnectedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iCarteDisconnected:) name:iCarteDisconnectedNotification object:nil];
    
    // create iCarte classes
    utilities = new Utilities();
    
    //flag
    timerIsStopped = YES;
}

- (void) startNFC
{
    objc_sync_enter(self);
    if (timerIsStopped==YES)
    {
        NSLog(@"Start NFC;");        
        timerIsStopped = NO;
        //initialize all taggers
        [self initializeTaggers];
        nfcTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        dispatch_source_set_timer(nfcTimer,dispatch_time(DISPATCH_TIME_NOW, 0), SCAN_INTERVAL*NSEC_PER_SEC, 0*NSEC_PER_SEC);
        dispatch_source_set_event_handler(nfcTimer, ^
        {
            [self scanNFC];
        });
        dispatch_resume(nfcTimer);
    }
    objc_sync_exit(self);
}

- (void) stopNFC
{
    objc_sync_enter(self);
    if (timerIsStopped == NO)
    {
        NSLog(@"Stop NFC;");
        [self cleanTaggers];
        if (nfcTimer)
        {
            dispatch_release(nfcTimer);
        }
        timerIsStopped = YES;
    }
    objc_sync_exit(self);
}

- (void) scanNFC
{
    @try
    {
        errorManager = ERR_NONE;
        if (utilities->IsiCarteConnected())
        {
            if((self.SCAN_TAG_TYPE & TAG_TYPE_ICODESLI) == TAG_TYPE_ICODESLI)
            {
                NSLog(@"TAG_TYPE_ICODESLI");
                
            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_KOVIO) == TAG_TYPE_KOVIO)
            {
                NSLog(@"TAG_TYPE_KOVIO");
            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_1K) == TAG_TYPE_MIFARE_1K)
            {
                NSLog(@"TAG_TYPE_MIFARE_1K");
            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_4K) == TAG_TYPE_MIFARE_4K)
            {
                NSLog(@"TAG_TYPE_MIFARE_4K");
            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_UL) == TAG_TYPE_MIFARE_UL)
            {
                NSLog(@"TAG_TYPE_MIFARE_UL");
            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_ULC) == TAG_TYPE_MIFARE_ULC)
            {
                NSLog(@"TAG_TYPE_MIFARE_ULC");
                errorManager = tagger_MifareULC->IsTagAvailable();
                switch (errorManager)
                {
                    case ERR_NONE:
                    {
                        break;
                    }
                    case ERR_TAG_AVAILABLE:
                    {
                        NSLog(@"Tag detected");
                        
                        //Vibrate
                        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                        
                        BOOL finishedReading = NO;
                        unsigned char contentBuffer[MUC_PAGE_SIZE];
                        unsigned char emptyLine[MUC_PAGE_SIZE] = {0x00,0x00,0x00,0x00};
                        NSMutableString * contentString = [[NSMutableString alloc] initWithCapacity:MUC_PAGE_SIZE*MUC_NUM_PAGES];
                        for (int page = MUC_FIRST_DATA_PAGE; page < MUC_LAST_DATA_PAGE; page++)
                        {
                            errorManager = tagger_MifareULC->ReadPage(page, contentBuffer);
                            switch (errorManager)
                            {
                                case ERR_NONE:
                                {
                                    for (int tmp = 0; tmp<MUC_PAGE_SIZE; tmp++) {
                                        if ( ( ((unsigned char)contentBuffer[tmp]) >= 0x20 ) && ( ((unsigned char)contentBuffer[tmp]) <= 0x7E ) )
                                            [contentString appendFormat:@"%c",contentBuffer[tmp]];
                                        if (((unsigned char)contentBuffer[tmp]) == 0xfe) {
                                            NSLog(@"Read finished at block %d", page);
                                            page = MUC_LAST_DATA_PAGE;
                                            tmp = MUC_PAGE_SIZE;
                                        }
                                    }
                                    
                                    
                                    if ([[NSString stringWithFormat:@"%s",contentBuffer] isEqualToString:[NSString stringWithFormat:@"%s",emptyLine]]) {
                                        NSLog(@"Read finished at block %d", page);
                                        page = MUC_LAST_DATA_PAGE;
                                    }
                                    if (page >= MUC_LAST_DATA_PAGE -1) {
                                        finishedReading = YES;
                                    }
                                    break;
                                }
                                case ERR_GEN_NULL_POINTER:
                                {
                                    NSLog(@"Null Pointer Exception while reading Tag Content in Block %d", page);
                                    page = MUC_LAST_DATA_PAGE;
                                    break;
                                }
                                case ERR_GEN_OUT_OF_RANGE:
                                {
                                    NSLog(@"Out of range Exception while reading Tag Content in Block %d", page);
                                    page = MUC_LAST_DATA_PAGE;
                                    break;
                                }
                                case ERR_GEN_NO_RESPONSE:
                                {
                                    NSLog(@"No response while reading Tag Content in Block %d", page);
                                    page = MUC_LAST_DATA_PAGE;
                                    break;
                                }
                                default:
                                    NSLog(@"Unknown error while reading Tag Content in Block %d: %ld", page,errorManager);
                                    page = MUC_LAST_DATA_PAGE;
                                    break;
                            }
                        }
                        if (finishedReading)
                        {
                            NSLog(@"Tag Content     = %@",contentString);
                            [self executeTagAction:contentString];
                            //Vibrate
                            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                        }
                        else
                        {
                            NSLog(@"Unable to read complete Tag");
                        }
                        
                        break;
                    }
                        
                    default:
                        
                        break;
                }

            }
            if((self.SCAN_TAG_TYPE & TAG_TYPE_TOPAZ) == TAG_TYPE_TOPAZ)
            {
                NSLog(@"TAG_TYPE_TOPAZ");
            }
        }
        else
        {
            NSLog(@"No iCarte connected");
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NO_NFC_DEVICE object:self userInfo:nil];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Unable to read Tag\nException: %@",exception);
    }
    @finally
    {
        
    }
}

- (void) executeTagAction:(NSString *)tagContent
{
    //pause the timer and relaunch it
    [self pauseTimer];
    double delayInSeconds = SCAN_PAUSE;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
       {
           [self relaunchTimer];
       });
    if(self.SCAN_TAG_MASK.length > 0)
    {
        if([tagContent rangeOfString:self.SCAN_TAG_MASK].location>0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NFC_DETECTED_FOUND_TAG object:tagContent userInfo:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NFC_DETECTED_UNKNOWN_TAG object:tagContent userInfo:nil];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NFC_DETECTED_FOUND_TAG object:tagContent userInfo:nil];        
    }
}

- (void) pauseTimer
{
    NSLog(@"Pause Timer");
    dispatch_suspend(nfcTimer);
}

- (void) relaunchTimer
{
    if (!timerIsStopped)
    {
        NSLog(@"Relaunch Timer");
        dispatch_resume(nfcTimer);
    }
}

- (BOOL) hasNFCDevice
{
    // check if iCarte connected
    return utilities->IsiCarteConnected();
}

- (void)initializeTaggers
{
    if((self.SCAN_TAG_TYPE & TAG_TYPE_ICODESLI) == TAG_TYPE_ICODESLI)
    {
        tagger_ICODESLI = new ICODESLI();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_KOVIO) == TAG_TYPE_KOVIO)
    {
        tagger_Kovio = new Kovio();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_1K) == TAG_TYPE_MIFARE_1K)
    {
        tagger_Mifare1K = new Mifare1K();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_4K) == TAG_TYPE_MIFARE_4K)
    {
        tagger_Mifare4K = new Mifare4K();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_UL) == TAG_TYPE_MIFARE_UL)
    {
        tagger_MifareUL = new MifareUL();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_MIFARE_ULC) == TAG_TYPE_MIFARE_ULC)
    {
        tagger_MifareULC = new MifareULC();
    }
    if((self.SCAN_TAG_TYPE & TAG_TYPE_TOPAZ) == TAG_TYPE_TOPAZ)
    {
        tagger_Topaz = new Topaz();
    }
}

- (void)cleanTaggers
{
    if(tagger_ICODESLI)
    {
        delete tagger_ICODESLI;
        tagger_ICODESLI = nil;
    }
    if(tagger_Kovio)
    {
        delete tagger_Kovio;
        tagger_Kovio = nil;
    }
    if(tagger_Mifare1K)
    {
        delete tagger_Mifare1K;
        tagger_Mifare1K = nil;
    }
    if(tagger_Mifare4K)
    {
        delete tagger_Mifare4K;
        tagger_Mifare4K = nil;
    }
    if(tagger_MifareUL)
    {
        delete tagger_MifareUL;
        tagger_MifareUL = nil;
    }
    if(tagger_MifareULC)
    {
        delete tagger_MifareULC;
        tagger_MifareULC = nil;
    }
    if(tagger_Topaz)
    {
        delete tagger_Topaz;
        tagger_Topaz = nil;
    }
}
#pragma mark -


#pragma mark iCarte management
- (void)iCarteConnected:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NFC_DEVICE_CONNECTED object:nil userInfo:nil];
}

- (void)iCarteDisconnected:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_NFC_DEVICE_DISCONNECTED object:nil userInfo:nil];
}
@end
