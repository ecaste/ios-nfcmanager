/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Mifare4K.h
*
* Description: Mifare Classic 4K header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __MIFARE4K_H__
#define __MIFARE4K_H__

#define M4K_UID_SIZE                   10    /**< Mifare Classic 4K Unique Identification (UID) number size in bytes. */

#define M4K_FIRST_SECTOR               0     /**< Mifare Classic 4K first available sector. */
#define M4K_LAST_SECTOR                39    /**< Mifare Classic 4K last available sector. */
#define M4K_NUM_SECTORS                40    /**< Mifare Classic 4K total number of sectors. */

#define M4K_FIRST_LOWER_SECTOR         0     /**< Mifare Classic 4K first available lower sector with 4 data blocks. */
#define M4K_LAST_LOWER_SECTOR          31    /**< Mifare Classic 4K last available lower sector with 4 data blocks. */
#define M4K_NUM_LOWER_SECTORS          32    /**< Mifare Classic 4K total number of lower sectors with 4 data blocks. */

#define M4K_FIRST_UPPER_SECTOR         32    /**< Mifare Classic 4K first available upper sector with 16 data blocks. */
#define M4K_LAST_UPPER_SECTOR          39    /**< Mifare Classic 4K last available upper sector with 16 data blocks. */
#define M4K_NUM_UPPER_SECTORS          8     /**< Mifare Classic 4K total number of upper sectors with 16 data blocks. */

#define M4K_FIRST_LOWER_BLOCK          0     /**< Mifare Classic 4K first available block in a lower sector with 4 data blocks. */
#define M4K_LAST_LOWER_BLOCK           3     /**< Mifare Classic 4K last available block in a lower sector with 4 data blocks. */
#define M4K_NUM_LOWER_BLOCKS           4     /**< Mifare Classic 4K number of blocks in a lower sector. */

#define M4K_FIRST_UPPER_BLOCK          0     /**< Mifare Classic 4K first available block in an upper sector with 16 data blocks. */
#define M4K_LAST_UPPER_BLOCK           15    /**< Mifare Classic 4K last available block in an upper sector with 16 data blocks. */
#define M4K_NUM_UPPER_BLOCKS           16    /**< Mifare Classic 4K number of blocks in an upper sector. */

#define M4K_BLOCK_SIZE                 16    /**< Mifare Classic 4K block size in bytes. */
#define M4K_KEY_SIZE                   6     /**< Mifare Classic 4K key size in bytes. */
#define M4K_ACCESS_SIZE                4     /**< Mifare Classic 4K access size in bytes. */

//! Mifare Classic 4K Key Type enumerator.
/**
 * Keys give you access to the data blocks and to the keys themselves. Each
 * sector has its own individual key. This means that all the sectors can have
 * different keys. If you write a new key and lose that key there is <b>NO</b>
 * way to get it back.
 */
typedef enum M4K_KEY_TYPE
{
   M4K_KEY_TYPE_A  = 0x00,                   /**< Key A will be used to authenticate the sector. */
   M4K_KEY_TYPE_B  = 0x01                    /**< Key B will be used to authenticate the sector. */
} M4K_KEY_TYPE;

class wMifare4K;

//! Mifare Classic 4K MF1ICS70.
/**
 * This class is designed to read and write to Mifare Classic 4K MF1ICS70 tags.
 * Care must be taken when writing data to these tags because writing data can
 * possibly cause the tag to be no longer accessible. It is important to read
 * the Mifare Classic 4K documentation first to better understand the features
 * and functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * Mifare Classic 4K tags are divided into 32 sectors with 4 blocks and 8
 * sectors with 16 blocks (one block consists of 16 bytes of data). Each sector
 * also contains 1 Sector Trailer except Sector 0 which also contains a
 * Manufacturer Block. The Manufacturer Block contains the IC Manufacturer Data
 * including the Serial Number (UID), Check Byte and Manufacturer Data. Sector
 * Trailers are also 16 bytes and contain secret Keys A and B and Access
 * bytes for the Data Blocks within that sector. You must know the Keys and
 * Access conditions for a given sector before you can access that sector's Data
 * Blocks or Sector Trailer.\n
 * \n
 * Below is a diagram of the Mifare Classic 4K (4096 X 8 bit) EEPROM organized
 * in 32 sectors with 4 blocks and in 8 sectors with 16 blocks. One block
 * consists of 16 bytes.\n
 * \n
 * <img src="Mifare4KMemory.jpg">
 * \n
 */
class Mifare4K
{
public:

   /*! \cond */
   Mifare4K();

   ~Mifare4K();
   /*! \endcond */

   /**
    * Locates a Mifare Classic 4K tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a Mifare Classic 4K tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a Mifare Classic 4K tag
    *            currently in the iCarte field. This buffer must have a minimum
    *            size of #M4K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid, int *uidLength);

   /**
    * Gets the Unique Identification (UID) number of the last Mifare Classic 4K
    * tag that was located by a call to IsTagAvailable(). This method differs
    * from ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last Mifare Classic
    *            4K tag that was located using IsTagAvailable(). This buffer
    *            must have a minimum size of #M4K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid, int *uidLength);

   /**
    * Reads the Unique Identification (UID) number of the Mifare Classic 4K tag
    * currently in the iCarte field.
    *
    * \param key is a buffer used for storing the key used for reading the UID.
    *            This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #M4K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *key, M4K_KEY_TYPE keyType, unsigned char *uid, int *uidLength);

   /**
    * Reads data from the specified sector data block of the Mifare Classic 4K
    * tag currently in the iCarte field.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to read from. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK when
    *              accessing lower sectors or from #M4K_FIRST_UPPER_BLOCK to
    *              #M4K_LAST_UPPER_BLOCK when accessing upper sectors.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #M4K_BLOCK_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadBlock(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType, unsigned char *readBuffer);

   /**
    * Writes data to the specified sector data block of the Mifare Classic 4K
    * tag currently in the iCarte field.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to write to. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK when
    *              accessing lower sectors or from #M4K_FIRST_UPPER_BLOCK to
    *              #M4K_LAST_UPPER_BLOCK when accessing upper sectors.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #M4K_BLOCK_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Care must be taken when writing data to the tag because writing data
    *       can possibly cause the tag to be no longer accessible. It is important
    *       to read the Mifare Classic 4K documentation first to better understand
    *       the features and functions of the tag.
    */
   ERR WriteBlock(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType, unsigned char *writeBuffer);

   /**
    * Increments the contents of a value block and stores the result in the
    * data register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to increment. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK or
    *              from #M4K_FIRST_UPPER_BLOCK to #M4K_LAST_UPPER_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Increment(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType, int32_t value);

   /**
    * Decrements the contents of a value block an stores the result in the data
    * register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to decrement. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK when
    *              accessing lower sectors or from #M4K_FIRST_UPPER_BLOCK to
    *              #M4K_LAST_UPPER_BLOCK when accessing upper sectors.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Decrement(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType, int32_t value);

   /**
    * Transfers the contents of the data register to a value block.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to transfer. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK or
    *              from #M4K_FIRST_UPPER_BLOCK to #M4K_LAST_UPPER_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Transfer(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType);

   /**
    * Restores the contents of a value block into the data register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M4K_FIRST_SECTOR to #M4K_LAST_SECTOR.
    *
    * \param block is the desired block to restore. Valid \a block values are
    *              from #M4K_FIRST_LOWER_BLOCK to #M4K_LAST_LOWER_BLOCK or
    *              from #M4K_FIRST_UPPER_BLOCK to #M4K_LAST_UPPER_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M4K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M4K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Restore(unsigned char sector, unsigned char block, unsigned char *key, M4K_KEY_TYPE keyType, int32_t value);

private:

   /*! \cond */
   wMifare4K  *wmifare4k;
   /*! \endcond */
};

#endif
