/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        ISO14443A_4.h
*
* Description: ISO/IEC 14443A-4 header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __ISO14443A_4_H__
#define __ISO14443A_4_H__

#define ISO14443A_4_UID_SINGLE_SIZE    4     /**< ISO/IEC 14443A-4 Unique Identification (UID) number single size in bytes. */
#define ISO14443A_4_UID_DOUBLE_SIZE    7     /**< ISO/IEC 14443A-4 Unique Identification (UID) number double size in bytes. */
#define ISO14443A_4_UID_TRIPLE_SIZE    10    /**< ISO/IEC 14443A-4 Unique Identification (UID) number triple size in bytes. */

#define ISO14443A_4_ATQA_LENGTH        2     /**< ISO/IEC 14443A-4 ATQA size in bytes. */
#define ISO14443A_4_SAK_LENGTH         1     /**< ISO/IEC 14443A-4 SAK size in bytes. */
#define ISO14443A_4_ATS_LENGTH         256   /**< ISO/IEC 14443A-4 ATS size in bytes. */

//! ISO 14443A-4 Bit Rate Divisor enumerator.
typedef enum ISO14443A_4_BIT_RATE_DIVISOR
{
   ISO14443A_4_BIT_RATE_DIVISOR_1  = 0x01,   /**< Default. A bit rate of 106 kbit/s shall be used. */
   ISO14443A_4_BIT_RATE_DIVISOR_2  = 0x02,   /**< A bit rate of 212 kbit/s shall be used. */
   ISO14443A_4_BIT_RATE_DIVISOR_4  = 0x04,   /**< A bit rate of 424 kbit/s shall be used. */
   ISO14443A_4_BIT_RATE_DIVISOR_8  = 0x08    /**< A bit rate of 848 kbit/s shall be used. */
} ISO14443A_4_BIT_RATE_DIVISOR;

//! ISO 14443A-4 Noise Attenuation enumerator.
typedef enum ISO14443A_4_NOISE_ATTENUATION
{
   ISO14443A_4_NOISE_ATTENUATION_OFF     = 0x00,   /**< Default. No noise attenuation shall be used. */
   ISO14443A_4_NOISE_ATTENUATION_LOW     = 0x04,   /**< Low noise attenuation shall be used. */
   ISO14443A_4_NOISE_ATTENUATION_MEDIUM  = 0x08,   /**< Medium noise attenuation shall be used. */
   ISO14443A_4_NOISE_ATTENUATION_HIGH    = 0x0C    /**< High noise attenuation shall be used. */
} ISO14443A_4_NOISE_ATTENUATION;

//! A structure to store the ISO 14443A Parameters.
typedef struct ISO14443AParameters
{
   ISO14443A_4_NOISE_ATTENUATION  na;        /**< Noise Attenuation. Some tags like DESFire and JCOP tags are very noisy and are hard to read at short distances. To increase reliability try increasing the noise attenuation. Increasing noise attenuation reduces range. */
   ISO14443A_4_BIT_RATE_DIVISOR   ds;        /**< Divisor Send. The bit rate of the PICC for the direction from PICC to PCD. Default is 106 kbit/s. */
   ISO14443A_4_BIT_RATE_DIVISOR   dr;        /**< Divisor Receive. The bit rate of the PICC for the direction from PDC to PICC. Default is 106 kbit/s. */
   unsigned char                  fwi;       /**< Frame Waiting Time Integer. The time within which a PICC shall start its response frame after the end of a PCD frame. Valid FWI values are from 0 to 14. Default value is 4. */
} ISO14443AParameters;

class wISO14443A_4;

//! ISO/IEC 14443-4 Type A Compliant Tags.
/**
 * This class is designed to locate ISO/IEC 14443A-4 compliant tags in the
 * iCarte field. This class handles the ISO/IEC 14443A-3 layer plus Request for
 * Answer To Select (RATS), Answer To Select (ATS) and Protocol and Parameter
 * Selection (PPS) request and response. Use SendCommand() and GetResponse()
 * to send transparent or proprietary data to the tag.\n
 * \n
 * Please refer to the ISO/IEC 14443-4 documentation for more information.\n
 * \n
 */
class ISO14443A_4
{
public:

   /*! \cond */
   ISO14443A_4();

   virtual ~ISO14443A_4();
   /*! \endcond */

   /**
    * Locates an ISO/IEC 14443A-4 compliant tag in the iCarte field. This method
    * must be called first before calling any other methods in this class which
    * access the tag. This method should be used in a looping mechanism so that
    * you can detect when a tag is in range. For maximum speed make sure no other
    * API methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag. This method
    * will send the CID set by SetCID() as part of the RATS command. If the tag
    * supports PPS and you want to send it you must send it right after calling
    * this method. Upon successful return of the PPS request you must call
    * SetParameters() with the correct DS, DR and FWI. If you find it hard
    * locating a tag try increasing the noise attenuation using the SetParameters()
    * method.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates an ISO/IEC 14443A-4 compliant tag in the iCarte field. This method
    * must be called first before calling any other methods in this class which
    * access the tag. This method should be used in a looping mechanism so that
    * you can detect when a tag is in range. For maximum speed make sure no other
    * API methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag. This method
    * will send the CID set by SetCID() as part of the RATS command. If the tag
    * supports PPS and you want to send it you must send it right after calling
    * this method. Upon successful return of the PPS request you must call
    * SetParameters() with the correct DS, DR and FWI. If you find it hard
    * locating a tag try increasing the noise attenuation using the SetParameters()
    * method.
    *
    * \param uid is a buffer used for storing the UID of an ISO/IEC 14443A-4
    *            compliant tag currently in the iCarte field. This buffer must
    *            have a minimum size of #ISO14443A_4_UID_SINGLE_SIZE but should
    *            have a size of #ISO14443A_4_UID_TRIPLE_SIZE
    *
    * \param uidLength for input the length of \a uid, for output the number
    *                  of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid, int *uidLength);

   /**
    * Selects the specified tag for further communication.
    *
    * \param uid is the uid of the tag to select.
    * \param uidLength is the length of \a uid. The valid lengths are
    *                  ISO14443A_3_UID_SINGLE_SIZE, ISO14443A_3_UID_DOUBLE_SIZE,
    *                  or ISO14443A_3_UID_TRIPLE_SIZE;
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO14443_COMMAND_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Select(unsigned char *uid, int uidLength);

   /**
    * Sends a deselect command without a Card Identifier (CID) to all ISO/IEC
    * 14443A-4 compliant tags in the iCarte field. After successful execution
    * of this command all tags that do not support CID or whose CID is 0 shall
    * be placed in the HALT state.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note If the error code returned from this method is #ERR_GEN_NO_RESPONSE
    *       you may retry.
    */
   ERR Deselect();

   /**
    * Deselects the specified ISO/IEC 14443A-4 compliant tag in the iCarte field.
    * After successful execution of this command the tag whose CID matches the
    * specified CID shall be placed in the HALT state.
    *
    * \param cid is the card identifier to set. Valid CID values are from 0 to 14.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_GEN_INCORRECT_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Deselect(unsigned char cid);

   /**
    * Gets the Unique Identification (UID) number of the last ISO/IEC 14443A-4
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ISO14443A_4_UID_SINGLE_SIZE but should have
    *            a size of #ISO14443A_4_UID_TRIPLE_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the number
    *                  of UID bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid, int *uidLength);

   /**
    * Gets the Answer To Request (ATQA) of the last ISO/IEC 14443A-4 compliant
    * tag that was located by a call to IsTagAvailable().
    *
    * \param atqa is a buffer used for storing the ATQA. This buffer must have a
    *             minimum size of #ISO14443A_4_ATQA_LENGTH. Please refer to the
    *             ISO/IEC 14443-3 specification for more information on how to
    *             decode this data.
    *
    * \param atqaLength for input the length of \a atqa, for output the number
    *                   of ATQA bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetATQA(unsigned char *atqa, int *atqaLength);

   /**
    * Gets the Select Acknowledge (SAK) of the last ISO/IEC 14443A-4 compliant
    * tag that was located by a call to IsTagAvailable().
    *
    * \param sak is a buffer used for storing the SAK. This buffer must have a
    *            minimum size of #ISO14443A_4_SAK_LENGTH. Please refer to the
    *            ISO/IEC 14443-3 specification for more information on how to
    *            decode this data.
    *
    * \param sakLength for input the length of \a sak, for output the number
    *                  of SAK bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetSAK(unsigned char *sak, int *sakLength);

   /**
    * Gets the Answer to Select (ATS) of the last ISO/IEC 14443A-4 compliant tag
    * that was located by a call to IsTagAvailable().
    *
    * \param ats is a buffer used for storing the ATS. This buffer must have a
    *            minimum size of #ISO14443A_4_ATS_LENGTH. Please refer to the
    *            ISO/IEC 14443-4 specification for more information on how to
    *            decode this data.
    *
    * \param atsLength for input the length of \a ats, for output the number
    *                  of ATS bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetATS(unsigned char *ats, int *atsLength);

   /**
    * Sets the Card Identifier (CID). CID defines the logical address of a
    * PICC in the iCarte field. CID is sent to a tag when a call to
    * IsTagAvailable() is made. CID shall be unique for all PICCs which are
    * active at the same time.
    *
    * \param cid is the card identifier to set. Valid CID values are from 0 to 14.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetCID(unsigned char cid);

   /**
    * Gets the current Card Identifier (CID).
    *
    * \param cid is used to store the current CID.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetCID(unsigned char *cid);

   /**
    * Sets the ISO14443A_4 class parameters. These parameters must be set in
    * order to properly communicate with an ISO/IEC 14443A-4 compliant tag. See
    * #ISO14443AParameters for more information on its parameters.
    *
    * \param parameters is the new parameters that will be set.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetParameters(ISO14443AParameters parameters);

   /**
    * Gets the current ISO14443A_4 class parameters.
    *
    * \param parameters is the reference used for storing the current ISO/IEC
    *                   14443A-4 parameters
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetParameters(ISO14443AParameters &parameters);

   /**
    * Sends a command (transparent or proprietary data) to the last ISO/IEC
    * 14443A-4 compliant tag located by a call to IsTagAvailable(). The \a
    * writeBuffer must contain the prologue field and information field but
    * need not contain the SOF, epilogue field (i.e. EDC/CRC_A/CRC1 and CRC2)
    * or EOF bytes.
    *
    * \param writeBuffer is a buffer used for storing the data to send to the
    *                    tag. The data must be in block format according to the
    *                    ISO/IEC 14443-4 specification. Do not forget to send
    *                    the prologue field which must contain the mandatory
    *                    Protocol Control Byte (PCB)!
    *
    * \param writeLength is the number of bytes to send to the tag. Do not exceed
    *                    the maximum frame size accepted by the PICC minus CRC1
    *                    and CRC2 (i.e. FSC - 2). If your data exceeds the maximum
    *                    frame size accepted by the PICC you must chain your data.
    *                    Please refer to the ISO/IEC 14443-4 specification for more
    *                    information on how to chain your data.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SendCommand(unsigned char *writeBuffer, int writeLength);

   /**
    * Gets a response from the last ISO/IEC 14443A-4 compliant tag located by a
    * call to IsTagAvailable(). The \a readBuffer contains the actual received
    * data from the tag with the SOF, CRC_A (i.e. CRC1 and CRC2) and EOF bytes
    * removed. If you find it hard communicating with a tag try increasing the
    * noise attenuation using the SetParameters() method. If you find it hard
    * receiving a response you may need to set the frame waiting time integer
    * using the SetParameters() method.
    *
    * \param readBuffer is a buffer used for storing the response. This buffer
    *                   must be at least \a readLength bytes in length.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetResponse(unsigned char *readBuffer, int *readLength);

private:

   /*! \cond */
   wISO14443A_4 *wiso14443A_4;
   /*! \endcond */
};

#endif
