/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Utilities.h
*
* Description: Utilities header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#define UTIL_UID_SIZE                  10    /**< iCarte Unique Identification (UID) number size in bytes. */
#define UTIL_PASSWORD_SIZE             16    /**< iCarte password size in bytes. */

#define UTIL_SERIAL_NUMBER_LENGTH      15    /**< iCarte Serial Number length. */
#define UTIL_SDK_VERSION_LENGTH        12    /**< iCarte SDK version length. */
#define UTIL_FIRMWARE_REVISION_LENGTH  12    /**< iCarte Firmware Revision length. */
#define UTIL_HARDWARE_REVISION_LENGTH  12    /**< iCarte Hardware Revision length. */
#define UTIL_MODEL_NUMBER_LENGTH       64    /**< iCarte Model Number length. */

static NSString * const iCarteConnectedNotification    = @"iCarteConnected";
static NSString * const iCarteDisconnectedNotification = @"iCarteDisconnected";

//! %Utilities Power Mode enumerator.
typedef enum UTIL_POWER_MODE
{
   UTIL_POWER_MODE_OFF      = 0x00,          /**< Off power mode. */
   UTIL_POWER_MODE_STANDBY  = 0x01,          /**< Standby power mode. */
   UTIL_POWER_MODE_IDLE     = 0x02,          /**< Idle power mode. */
   UTIL_POWER_MODE_ACTIVE   = 0x03           /**< Active power mode. */
} UTIL_POWER_MODE;

//! %Utilities Field Detect Event enumerator.
typedef enum UTIL_FIELD_DETECT_EVENT
{
   UTIL_FIELD_DETECT_EVENT_EXIT   = 0x01,    /**< Request to be notified only when the iCarte has exited the field of another reader. */
   UTIL_FIELD_DETECT_EVENT_ENTER  = 0x02     /**< Request to be notified only when the iCarte has entered the field of another reader. */
} UTIL_FIELD_DETECT_EVENT;

//! %Utilities Secure Element Contactless enumerator.
typedef enum UTIL_CONTACTLESS
{
   UTIL_CONTACTLESS_OFF  = 0x00,             /**< Secure Element Contactless is off. */
   UTIL_CONTACTLESS_ON   = 0x01,             /**< Secure Element Contactless is on. */
} UTIL_CONTACTLESS;

//! Authentication Status
/**
 * %Utilities iCarte %Crosscheck Lock Status.
 */
typedef struct AuthenticationStatus
{
   uint16_t uid               :1;            /**< UID Locked/Unlocked. */
   uint16_t uuid              :1;            /**< UUID Locked/Unlocked. */
   uint16_t rfu_2             :1;            /**< Reserved for Future Use. */
   uint16_t carrierName       :1;            /**< Carrier Name Locked/Unlocked. */
   uint16_t rfu_4             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_5             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_6             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_7             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_8             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_9             :1;            /**< Reserved for Future Use. */
   uint16_t rfid              :1;            /**< RFID Locked/Unlocked. */
   uint16_t secureElement     :1;            /**< Secure Element Locked/Unlocked. */
   uint16_t utilities         :1;            /**< %Utilities Locked/Unlocked. */
   uint16_t rfu_D             :1;            /**< Reserved for Future Use. */
   uint16_t rfu_E             :1;            /**< Reserved for Future Use. */
   uint16_t permanentLockout  :1;            /**< Permanent Lock Out Locked/Unlocked. */
} AuthenticationStatus;

class wUtilities;

//! %Utilities
/**
 * This class provides utility functions required to interact with the iCarte.
 */
class Utilities
{
public:

   /*! \cond */
   Utilities();

   ~Utilities();
   /*! \endcond */

   /**
    * Returns a boolean indicating whether the iCarte is connected to the iPhone.
    * If this method returns \a true you can begin calling iCarte API methods.
    * You should not call iCarte API methods if the iCarte is not connected.
    * Please note that even though the iCarte may be physically attached to the
    * iPhone it may not be connected. After attaching the iCarte to the iPhone
    * the iOS typically takes 2 seconds to connect.\n
    * \n
    * Alternatively you may register to receive iCarteConnected and iCarteDisconnected
    * notifications from the iOS. To do this use the addObserver() method using
    * iCarteConnectedNotification and iCarteDisconnectedNotification as the
    * notification names. Be sure to call removeObserver() when you are done.
    * Please refer to the NSNotificationCenter Class Reference for more detailed
    * information. Please note that if the iCarte is already connected to the
    * iPhone before your application is started you will not receive a notification
    * so you must call IsiCarteConnected() at least once when your application
    * starts to determine if the iCarte is connected.
    *
    * \retval true if the iCarte is connected.
    * \retval false if the iCarte is not connected.
    */
   bool IsiCarteConnected();

   /**
    * Gets the Authentication status of the currently connected iCarte. The Authentication
    * Status contains the bit field representation of locked or unlocked elements such as:
    * UID, UUID, IMSI, Carrier Name and Customer Passwords for RFID, Secure Element, and %Utilities.
    *
    *
    * \param status is a buffer used for storing the Authentication status.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetAuthenticationStatus(AuthenticationStatus *status);

   /**
    * Authenticates with the iCarte to be able to authenticate against the
    * Customer Username and Password Levels 1, 2, and 3.
    * Depending on which Customer Password Index each security element
    * (SE, RFID, %Utilities, or %Crosscheck) is set to, you may need to call this
    * method multiple times in order to unlock each feature.
    *
    * \param username is a buffer used for storing the authentication username.
    *                This buffer must have a minimum size of
    *                #CROSSCHECK_MINIMUM_SIZE.
    *
    * \param usernameLength will contain the size of the value.
    *                The length must not exceed #CROSSCHECK_USERNAME_SIZE.
    *
    * \param password is a buffer used for storing the authentication password.
    *                This buffer must have a minimum size of
    *                #CROSSCHECK_MINIMUM_SIZE.
    *
    * \param passwordLength will contain the size of the value.
    *                The length must not exceed #CROSSCHECK_PASSWORD_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_INCORRECT_PASSWORD
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Authenticate(uint8_t *username, int usernameLength, uint8_t *password, int passwordLength);

   /**
    * Sets the iCarte power. Use this method to change the iCarte power
    * settings. Each setting has different battery life and performance
    * features. It is recommended you set the power mode to #UTIL_POWER_MODE_OFF
    * when the applicationWillResignActive method is called in your application
    * delegate. Once the power mode is set to #UTIL_POWER_MODE_OFF you must
    * set it to #UTIL_POWER_MODE_ACTIVE before calling any other API method.
    * It is recommended you set the power mode to #UTIL_POWER_MODE_ACTIVE when
    * the applicationDidBecomeActive method is called in your application
    * delegate.
    *
    * \param power is the new power setting. For more information see
    *              #UTIL_POWER_MODE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_API_FEATURE_NOT_IMPLEMENTED
    * \retval #ERR_GEN_FW_UPGRADE_REQUIRED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetPower(UTIL_POWER_MODE power);

   /**
    * Gets the SDK version. The SDK version is returned as an ASCII formatted
    * string of the form "X.Y.Z".
    *
    * \param version is a buffer used for storing the SDK version. This buffer
    *                must have a minimum size of #UTIL_SDK_VERSION_LENGTH.
    *
    * \param versionLength for input the length of \a version, for output the
    *                      length of the SDK version string (including NULL
    *                      terminator).
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetSDKVersion(char *version, int *versionLength);

   /**
    * Gets the UID of the currently connected iCarte. The length of the UID is
    * either 4, 7, or 10 bytes.
    *
    * \param uid is a buffer used for storing the UID of the iCarte. This buffer
    *            must have a minimum size of #UTIL_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the number
    *                  of UID bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(uint8_t *uid, int *uidLength);

   /**
    * Gets the serial number of the currently connected iCarte. The serial
    * number is returned as an ASCII formatted string.
    *
    * \param serialNumber is a buffer used for storing the iCarte serial number.
    *                     This buffer must have a minimum size of
    *                     #UTIL_SERIAL_NUMBER_LENGTH.
    *
    * \param serialNumberLength for input the length of \a serialNumber, for
    *                           output the length of the serial number string
    *                           (including NULL terminator).
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetSerialNumber(char *serialNumber, int *serialNumberLength);

   /**
    * Gets the firmware revision of the currently connected iCarte. The firmware
    * revision is returned as an ASCII formatted string of the form "X.Y.Z".
    *
    * \param firmwareRevision is a buffer used for storing the iCarte firmware
    *                         revision. This buffer must have a minimum size of
    *                         #UTIL_FIRMWARE_REVISION_LENGTH.
    *
    * \param firmwareRevisionLength for input the length of \a firmwareRevision,
    *                               for output the length of the firmware
    *                               revision string (including NULL terminator).
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetFirmwareRevision(char *firmwareRevision, int *firmwareRevisionLength);

   /**
    * Gets the hardware revision of the currently connected iCarte. The hardware
    * revision is returned as an ASCII formatted string of the form "X.Y.Z".
    *
    * \param hardwareRevision is a buffer used for storing the iCarte hardware
    *                         revision. This buffer must have a minimum size of
    *                         #UTIL_HARDWARE_REVISION_LENGTH.
    *
    * \param hardwareRevisionLength for input the length of \a hardwareRevision,
    *                               for output the length of the hardware
    *                               revision string (including NULL terminator).
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetHardwareRevision(char *hardwareRevision, int *hardwareRevisionLength);

   /**
    * Gets the model number of the currently connected iCarte. The model number
    * is returned as an ASCII formatted string.
    *
    * \param modelNumber is a buffer used for storing the iCarte model number.
    *                    This buffer must have a minimum size of
    *                    #UTIL_MODEL_NUMBER_LENGTH.
    *
    * \param modelNumberLength for input the length of \a modelNumber, for
    *                          output the length of the model number string
    *                          (including NULL terminator).
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetModelNumber(char *modelNumber, int *modelNumberLength);

   /**
    * Enables the iCarte field detect. A field detect event occurs when the
    * iCarte enters or exits the field of another RFID reader. After calling
    * this method you can begin calling CheckFieldDetect() to determine
    * if an event has occurred. Always call DisableFieldDetect() when you no
    * longer want to detect field events. Field detect is automatically disabled
    * after a field detect event occurs.
    *
    * \param fieldDetectEvent is the type of field detect event the iCarte
    *                         should detect. For more information see
    *                         #UTIL_FIELD_DETECT_EVENT.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR EnableFieldDetect(UTIL_FIELD_DETECT_EVENT fieldDetectEvent);

   /**
    * Disables the iCarte field detect. See EnableFieldDetect().
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR DisableFieldDetect();

   /**
    * Checks for a field detect event. You must call EnableFieldDetect() before
    * calling this method. Field detect is automatically disabled after a field
    * detect event occurs.
    *
    * \param timeInField is the time in seconds that the iCarte was in the field
    *                    of another RFID reader.
    *
    * \retval #ERR_UTIL_FIELD_DETECT_NO_EVENT
    * \retval #ERR_UTIL_FIELD_DETECT_EVENT_EXIT
    * \retval #ERR_UTIL_FIELD_DETECT_EVENT_ENTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR CheckFieldDetect(float &timeInField);

   /**
    * Sets the state of the Secure Element contactless interface. After calling
    * this method the Secure Element contactless interface is set the value
    * of \a contactless. If \a contactless is set to #UTIL_CONTACTLESS_ON then
    * the Secure Element contactless interface is on and you can access the
    * Secure Element using an external reader or the ISO14443A_4 class. If
    * \a contactless is set to #UTIL_CONTACTLESS_OFF then the Secure Element
    * contactless interface is off and you cannot access the Secure Element using
    * an external reader or the ISO14443A_4 class. For security reasons it is
    * recommended that the Secure Element contactless interface be set to
    * #UTIL_CONTACTLESS_OFF when not in use.
    *
    * \param contactless is the state to set the Secure Element contactless
    *                    interface. The possible values are #UTIL_CONTACTLESS_ON
    *                    or #UTIL_CONTACTLESS_OFF.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Calling a method in any other class may change the state of the
    *       Secure Element contactless interface.
    */
   ERR SetContactless(UTIL_CONTACTLESS contactless);

   /**
    * Gets the state of the Secure Element contactless interface.
    *
    * \param contactless is the current state of the Secure Element contactless
    *                    interface. The possible values are #UTIL_CONTACTLESS_ON
    *                    or #UTIL_CONTACTLESS_OFF.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_FW_UPGRADE_REQUIRED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetContactless(UTIL_CONTACTLESS &contactless);


private:

   /*! \cond */
   wUtilities *wutilities;
   /*! \endcond */
};

#endif
