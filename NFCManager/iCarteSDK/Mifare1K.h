/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Mifare1K.h
*
* Description: Mifare Classic 1K header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __MIFARE1K_H__
#define __MIFARE1K_H__

#define M1K_UID_SIZE                   10    /**< Mifare Classic 1K Unique Identification (UID) number size in bytes. */

#define M1K_FIRST_SECTOR               0     /**< Mifare Classic 1K first available sector. */
#define M1K_LAST_SECTOR                15    /**< Mifare Classic 1K last available sector. */
#define M1K_NUM_SECTORS                16    /**< Mifare Classic 1K total number of sectors. */

#define M1K_FIRST_BLOCK                0     /**< Mifare Classic 1K first available block. */
#define M1K_LAST_BLOCK                 3     /**< Mifare Classic 1K last available block. */
#define M1K_NUM_BLOCKS                 4     /**< Mifare Classic 1K total number of blocks per sector. */

#define M1K_BLOCK_SIZE                 16    /**< Mifare Classic 1K block size in bytes. */
#define M1K_KEY_SIZE                   6     /**< Mifare Classic 1K key size in bytes. */
#define M1K_ACCESS_SIZE                4     /**< Mifare Classic 1K access size in bytes. */

//! Mifare Classic 1K Key Type enumerator.
/**
 * Keys give you access to the data blocks and to the keys themselves. Each
 * sector has its own individual key. This means that all the sectors can have
 * different keys. If you write a new key and lose that key there is <b>NO</b>
 * way to get it back.
 */
typedef enum M1K_KEY_TYPE
{
   M1K_KEY_TYPE_A  = 0x00,                   /**< Key A will be used to authenticate the sector. */
   M1K_KEY_TYPE_B  = 0x01                    /**< Key B will be used to authenticate the sector. */
} M1K_KEY_TYPE;

class wMifare1K;

//! Mifare Classic 1K MF1ICS50.
/**
 * This class is designed to read and write to Mifare Classic 1K MF1ICS50 tags.
 * Care must be taken when writing data to these tags because writing data can
 * possibly cause the tag to be no longer accessible. It is important to read
 * the Mifare Classic 1K documentation first to better understand the features
 * and functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * Mifare Classic 1K tags are divided into sectors. Each sector contains 3 Data
 * Blocks and 1 Sector Trailer except Sector 0 which contains a Manufacturer
 * Block, 2 Data Blocks and a Sector Trailer. The Manufacturer Block contains
 * the IC Manufacturer Data including the Serial Number (UID), Check Byte and
 * Manufacturer Data. Data Blocks are 16 bytes and are used for storing data.
 * Sector Trailers are also 16 bytes and contain secret Keys A and B and Access
 * bytes for the Data Blocks within that sector. You must know the Keys and
 * Access conditions for a given sector before you can access that sector's Data
 * Blocks or Sector Trailer.\n
 * \n
 * Below is a diagram of the Mifare Classic 1K (1024 X 8 bit) EEPROM organized
 * in 16 sectors with 4 blocks of 16 bytes each.\n
 * \n
 * <img src="Mifare1KMemory.jpg">
 * \n
 */
class Mifare1K
{
public:

   /*! \cond */
   Mifare1K();

   ~Mifare1K();
   /*! \endcond */

   /**
    * Locates a Mifare Classic 1K tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a Mifare Classic 1K tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a Mifare Classic 1K tag
    *            currently in the iCarte field. This buffer must have a minimum
    *            size of #M1K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid, int *uidLength);

   /**
    * Gets the Unique Identification (UID) number of the last Mifare Classic 1K
    * tag that was located by a call to IsTagAvailable(). This method differs
    * from ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last Mifare Classic
    *            1K tag that was located using IsTagAvailable(). This buffer
    *            must have a minimum size of #M1K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid, int *uidLength);

   /**
    * Reads the Unique Identification (UID) number of the Mifare Classic 1K tag
    * currently in the iCarte field.
    *
    * \param key is a buffer used for storing the key used for reading the UID.
    *            This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #M1K_UID_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *key, M1K_KEY_TYPE keyType, unsigned char *uid, int *uidLength);

   /**
    * Reads data from the specified sector data block of the Mifare Classic 1K
    * tag currently in the iCarte field.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to read from. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #M1K_BLOCK_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadBlock(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType, unsigned char *readBuffer);


   /**
    * Writes data to the specified sector data block of the Mifare Classic 1K
    * tag currently in the iCarte field.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to write to. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #M1K_BLOCK_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Care must be taken when writing data to the tag because writing data
    *       can possibly cause the tag to be no longer accessible. It is important
    *       to read the Mifare Classic 1K documentation first to better understand
    *       the features and functions of the tag.
    */
   ERR WriteBlock(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType, unsigned char *writeBuffer);

   /**
    * Increments the contents of a value block and stores the result in the
    * data register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to increment. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Increment(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType, int32_t value);

   /**
    * Decrements the contents of a value block an stores the result in the data
    * register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to decrement. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Decrement(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType, int32_t value);

   /**
    * Transfers the contents of the data register to a value block.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to transfer. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Transfer(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType);

   /**
    * Restores the contents of a value block into the data register.
    *
    * \param sector is the desired sector to access. Valid \a sector values
    *               are from #M1K_FIRST_SECTOR to #M1K_LAST_SECTOR.
    *
    * \param block is the desired block to restore. Valid \a block values are
    *              from #M1K_FIRST_BLOCK to #M1K_LAST_BLOCK.
    *
    * \param key is a buffer used for storing the key used to authenticate the
    *            sector. This buffer must have a size of #M1K_KEY_SIZE.
    *
    * \param keyType is which key type to use to access the sector. For more
    *                information see #M1K_KEY_TYPE.
    *
    * \param value is the signed 4 byte value. This value must be in the correct
    *              value block format for this command to work.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_CRC_ERROR
    * \retval #ERR_MIFARE_AUTHENTICATION_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Restore(unsigned char sector, unsigned char block, unsigned char *key, M1K_KEY_TYPE keyType, int32_t value);

private:

   /*! \cond */
   wMifare1K  *wmifare1k;
   /*! \endcond */
};

#endif
