/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        ISO15693_3.h
*
* Description: ISO/IEC 15693-3 header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __ISO15693_3_H__
#define __ISO15693_3_H__

#define ISO15693_3_UID_SIZE                    8      /**< ISO/IEC 15693-3 Unique Identification (UID) number size in bytes. */

#define ISO15693_3_BLOCK_SECURITY_STATUS_SIZE  1      /**< ISO/IEC 15693-3 Block Security Status size in bytes. */

#define ISO15693_3_TAG_INFO_DSFID_MASK         0x01   /**< Determines #ISO15693TagInfo::dsfid validity when masked with #ISO15693TagInfo::infoFlags. */
#define ISO15693_3_TAG_INFO_AFI_MASK           0x02   /**< Determines #ISO15693TagInfo::afi validity when masked with #ISO15693TagInfo::infoFlags. */
#define ISO15693_3_TAG_INFO_MEMORY_MASK        0x04   /**< Determines #ISO15693TagInfo::numberOfBlocks and #ISO15693TagInfo::blockSize validity when masked with #ISO15693TagInfo::infoFlags. */
#define ISO15693_3_TAG_INFO_ICREF_MASK         0x08   /**< Determines #ISO15693TagInfo::icReference validity when masked with #ISO15693TagInfo::infoFlags. */

//! ISO 15693-3 Modulation enumerator.
typedef enum ISO15693_3_MODULATION
{
   ISO15693_3_MODULATION_10_PERCENT_ASK   = 0x00,  /**< 10% ASK modulation index shall be used. */
   ISO15693_3_MODULATION_100_PERCENT_ASK  = 0x01   /**< 100% ASK modulation index shall be used. */
} ISO15693_3_MODULATION;

//! ISO 15693-3 Data Coding enumerator.
typedef enum ISO15693_3_DATA_CODING
{
   ISO15693_3_DATA_CODING_1_OF_4    = 0x00,      /**< 1 out of 4 data coding shall be used. */
   ISO15693_3_DATA_CODING_1_OF_256  = 0x01       /**< 1 out of 256 data coding shall be used. */
} ISO15693_3_DATA_CODING;

//! ISO 15693-3 Subcarrier enumerator.
typedef enum ISO15693_3_SUBCARRIER
{
   ISO15693_3_SUBCARRIER_SINGLE  = 0x00,         /**< Single subcarrier shall be used. */
   ISO15693_3_SUBCARRIER_DUAL    = 0x01          /**< Dual subcarrier shall be used. */
} ISO15693_3_SUBCARRIER;

//! ISO 15693-3 Data Rate enumerator.
typedef enum ISO15693_3_DATA_RATE
{
   ISO15693_3_DATA_RATE_LOW   = 0x00,            /**< Low data rate shall be used. */
   ISO15693_3_DATA_RATE_HIGH  = 0x02             /**< High data rate shall be used. */
} ISO15693_3_DATA_RATE;

//! ISO 15693-3 Request State enumerator.
typedef enum ISO15693_3_REQUEST_STATE
{
   ISO15693_3_REQUEST_STATE_BROADCAST  = 0x00,   /**< Request is not addressed. UID field is not included. It shall be executed by any VICC. */
   ISO15693_3_REQUEST_STATE_SELECTED   = 0x10,   /**< Request shall be executed only by a VICC in selected state. UID field shall not be included in the request. */
   ISO15693_3_REQUEST_STATE_ADDRESSED  = 0x20    /**< Request is addressed. UID field is included. It shall be executed only by the VICC whose UID matches the UID specified in the request. */
} ISO15693_3_REQUEST_STATE;

//! ISO 15693-3 Option Flag enumerator.
typedef enum ISO15693_3_OPTION_FLAG
{
   ISO15693_3_OPTION_FLAG_NO   = 0x00,           /**< Option flag is not set. */
   ISO15693_3_OPTION_FLAG_YES  = 0x40            /**< Option flag is set. */
} ISO15693_3_OPTION_FLAG;

//! A structure to store ISO 15693-3 Parameters.
typedef struct ISO15693Parameters
{
   ISO15693_3_MODULATION     modulation;         /**< ISO/IEC 15693-3 modulation index for communication between the VCD and the VICC. */
   ISO15693_3_DATA_CODING    dataCoding;         /**< ISO/IEC 15693-3 data coding. */
   ISO15693_3_SUBCARRIER     subcarrier;         /**< ISO/IEC 15693-3 subcarrier. */
   ISO15693_3_DATA_RATE      dataRate;           /**< ISO/IEC 15693-3 request data rate. */
   ISO15693_3_REQUEST_STATE  requestState;       /**< ISO/IEC 15693-3 request state. */
   ISO15693_3_OPTION_FLAG    optionFlag;         /**< ISO/IEC 15693-3 request option flag. */
} ISO15693Parameters;

//! A structure to store the ISO 15693-3 Tag Information.
typedef struct ISO15693TagInfo
{
   unsigned char  uid[ISO15693_3_UID_SIZE];      /**< Unique Identification (UID) number. */
   unsigned char  infoFlags;                     /**< This byte is used to determine which system information fields are present on the tag. */
   unsigned char  dsfid;                         /**< Data Storage Format Identifier (DSFID). For more information on the DSFID please refer to the ISO/IEC 15693-3 specification. */
   unsigned char  afi;                           /**< Application Family Identifier (AFI). For more information on the AFI please refer to the ISO/IEC 15693-3 specification. */
   int            numberOfBlocks;                /**< Number of actual User Data blocks on the tag. */
   unsigned char  blockSize;                     /**< Actual User Data block size in bytes. */
   unsigned char  icReference;                   /**< IC Reference identifier for the tag as defined by the IC manufacturer. */
} ISO15693TagInfo;

class wISO15693_3;

//! ISO/IEC 15693-3 Compliant Tag.
/**
 * This class is designed to locate ISO/IEC 15693-3 compliant tags in the
 * iCarte field. This class will handle the tag initialization, anticollision
 * and transmission protocols. Use SendCommand() and GetResponse() to send
 * proprietary protocols and commands.\n
 * \n
 * Please refer to the ISO/IEC 15693-3 documentation for more information.\n
 * \n
 */
class ISO15693_3
{
public:

   /*! \cond */
   ISO15693_3();

   virtual ~ISO15693_3();
   /*! \endcond */

   /**
    * Locates an ISO/IEC 15693-3 compliant tag in the iCarte field. This method
    * must be called first before calling any other methods in this class which
    * access the tag. This method should be used in a looping mechanism so that
    * you can detect when a tag is in range. For maximum speed make sure no other
    * API methods are called until a tag is found. Once a tag is available you
    * may start calling other methods in this class which access the tag. After a
    * tag is located it is automatically selected.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates an ISO/IEC 15693-3 compliant tag with the specified Application
    * Family Identifier (AFI) in the iCarte field. This method must be called
    * first before calling any other methods in this class which access the
    * tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag. After a
    * tag is located it is automatically selected.
    *
    * \param afi is the Application Family Identifier (AFI). Only tags with a
    *            matching AFI will respond to this method. For more information
    *            on the AFI please refer to the ISO/IEC 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char afi);

   /**
    * Gets the Unique Identification (UID) number of the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable(). This method
    * differs from ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ISO15693_3_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number from the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ISO15693_3_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Sets the tag information. Only #ISO15693TagInfo::blockSize and
    * #ISO15693TagInfo::numberOfBlocks are updated and they only need to be set
    * if they are invalid after a call to ReadTagInfo().
    *
    * \param tagInfo is the structure which holds the #ISO15693TagInfo::blockSize
    *                and #ISO15693TagInfo::numberOfBlocks values to set.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetTagInfo(ISO15693TagInfo tagInfo);

   /**
    * Gets the tag information of the last ISO/IEC 15693-3 compliant tag that was
    * located by a call to IsTagAvailable(). This method differs from
    * ReadTagInfo() because it doesn't read any data from the tag.
    *
    * \param tagInfo is the reference used for storing the tag information.
    *                See #ISO15693TagInfo for more information on its parameters.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetTagInfo(ISO15693TagInfo &tagInfo);

   /**
    * Reads the tag information from the last ISO/IEC 15693-3 compliant tag that
    * was located by a call to IsTagAvailable().
    *
    * \param tagInfo is the reference used for storing the tag information.
    *                See #ISO15693TagInfo for information on its parameters.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadTagInfo(ISO15693TagInfo &tagInfo);

   /**
    * Reads data from the specified User Data block from the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param block is the desired block to read from. Valid \a block values are
    *              from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param readBuffer is a buffer used for storing the Block Security Status
    *                   and or the User Block data. This buffer must have a
    *                   minimum size of #ISO15693TagInfo::blockSize. If the
    *                   #ISO15693Parameters::optionFlag is set you must increase the
    *                   buffer size by #ISO15693_3_BLOCK_SECURITY_STATUS_SIZE so
    *                   that the Block Security Status can be inserted as the
    *                   first byte in \a readBuffer.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadBlock(int block, unsigned char *readBuffer, int *readLength);

   /**
    * Reads data from multiple User Data blocks from the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param firstBlock is the first block to read from. Valid \a block values
    *                    are from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param numberOfBlocks is the number of blocks to read. Valid values are
    *                       from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param readBuffer is a buffer used for storing the Block Security Status
    *                   and or the User Block data. This buffer must have a
    *                   minimum size of #ISO15693TagInfo::blockSize * \a
    *                   numberOfBlocks. If the #ISO15693Parameters::optionFlag is
    *                   set you must increase the buffer size by \a numberOfBlocks
    *                   so that the Block Security Status can be inserted before
    *                   each User Block data. The Block Security Status followed
    *                   by the User Block data will be repeated \a numberOfBlocks
    *                   for each block.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadMultipleBlocks(int firstBlock, int numberOfBlocks, unsigned char *readBuffer, int *readLength);

   /**
    * Writes data to the specified User Data block of the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param block is the desired block to write to. Valid \a block values are
    *              from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param writeBuffer is the  buffer used for storing the User Block data
    *                    to write to the tag. This buffer must have a minimum
    *                    size of #ISO15693TagInfo::blockSize.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteBlock(int block, unsigned char *writeBuffer);

   /**
    * Writes data to multiple User Data blocks of the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable().
    *
    * \param firstBlock is the first block to write to. Valid \a block values are
    *                   from 0 to #ISO15693TagInfo::numberOfBlocks - \a
    *                   numberOfBlocks.
    *
    * \param numberOfBlocks is the number of blocks to write. Valid \a block values
    *                       are from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param writeBuffer is the  buffer used for storing the User Block data
    *                    to write to the tag. This buffer must have a minimum
    *                    size of #ISO15693TagInfo::blockSize *
    *                    #ISO15693TagInfo::numberOfBlocks.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteMultipleBlocks(int firstBlock, int numberOfBlocks, unsigned char *writeBuffer);

   /**
    * Locks the specified User Data block of the last ISO/IEC 15693-3 compliant
    * tag that was located by a call to IsTagAvailable(). It is important to
    * note that when a block on the tag has been locked it can never be written
    * to again. It is not possible to unlock a block once it has been locked.
    *
    * \param block is the desired block to lock. Valid \a block values are from 0
    *              to #ISO15693TagInfo::numberOfBlocks.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockBlock(int block);

   /**
    * Writes the Data Storage Format Identifier (DSFID) to the last ISO/IEC
    * 15693-3 compliant tag that was located by a call to IsTagAvailable(). For
    * more information about the DSFID please refer to the ISO/IEC 15693-3
    * specification.
    *
    * \param dsfid is the DSFID to write to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteDSFID(unsigned char dsfid);

   /**
    * Locks the Data Storage Format Identifier (DSFID) of the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable(). It is
    * important to note that when the DSFID on the tag has been locked it can
    * never be written to again. It is not possible to unlock the DSFID once it
    * has been locked. Make necessary changes to the DSFID using WriteDSFID()
    * before calling this method. For more information on the DSFID please refer
    * to the ISO/IEC 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockDSFID();

   /**
    * Writes the Application Family Identifier (AFI) to the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable(). For more
    * information about the AFI please refer to the ISO/IEC 15693-3
    * specification.
    *
    * \param afi is the AFI to write to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteAFI(unsigned char afi);

   /**
    * Locks the Application Family Identifier (AFI) of the last ISO/IEC 15693-3
    * compliant tag that was located by a call to IsTagAvailable(). It is
    * important to note that when the AFI on the tag has been locked it can
    * never be written to again. It is not possible to unlock the AFI once it
    * has been locked. Make necessary changes to the AFI using WriteAFI() before
    * calling this method. For more information on the AFI please refer to the
    * ISO/IEC 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockAFI();

   /**
    * Reads multiple User Data locks from the last ISO/IEC 15693-3 compliant tag
    * that was located by a call to IsTagAvailable().
    *
    * \param firstBlock is the first block to read from. Valid \a block values
    *                   are from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param numberOfBlocks is the number of blocks to read. Valid values are
    *                       from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param readBuffer is a buffer used to store the lock data. This buffer
    *                   must have a minimum size of \a numberOfBlocks. Each byte
    *                   in \a readBuffer corresponds to a block on the tag. If
    *                   the corresponding byte is 0x00 then the block is unlocked.
    *                   All other values denote a locked block.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadLocks(int firstBlock, int numberOfBlocks, unsigned char *readBuffer, int *readLength);

   /**
    * Writes multiple User Data locks to the last ISO/IEC 15693-3 compliant tag
    * that was located by a call to IsTagAvailable(). It is important to note
    * that when a block on the tag has been locked it can never be written to
    * again. It is not possible to unlock a block once it has been locked.
    *
    * \param firstBlock is the first block to lock. Valid \a block values are
    *                   from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \param numberOfBlocks is the number of blocks to lock. Valid values are
    *                       from 0 to #ISO15693TagInfo::numberOfBlocks.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR WriteLocks(int firstBlock, int numberOfBlocks);

   /**
    * Sets the ISO15693_3 class parameters. These parameters must be set in
    * order to properly communicate with an ISO/IEC 15693-3 compliant tag. See
    * #ISO15693Parameters for more information on its parameters.
    *
    * \param parameters is the new parameters that will be set.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetParameters(ISO15693Parameters parameters);

   /**
    * Gets the current ISO15693_3 class parameters.
    *
    * \param parameters is the reference used for storing the current ISO/IEC
    *                   15693-3 parameters
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetParameters(ISO15693Parameters &parameters);

   /**
    * Sends a command (transparent or proprietary data) to the last ISO/IEC
    * 15693-3 compliant tag located by a call to IsTagAvailable(). The \a
    * writeBuffer need not contain the SOF, CRC16 or EOF bytes.
    *
    * \param writeBuffer is a buffer used for storing the data to send to the
    *                    tag.
    *
    * \param writeLength is the number of bytes to send to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SendCommand(unsigned char *writeBuffer, int writeLength);

   /**
    * Gets a response from the last ISO/IEC 15693-3 compliant tag located by a
    * call to IsTagAvailable(). The \a readBuffer contains the actual received
    * data from the tag with the SOF, CRC16 and EOF bytes removed.
    *
    * \param readBuffer is a buffer used for storing the response. This buffer
    *                   must be at least \a readLength bytes in length.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetResponse(unsigned char *readBuffer, int *readLength);

private:

   /*! \cond */
   wISO15693_3 *wiso15693_3;
   /*! \endcond */
};

#endif
