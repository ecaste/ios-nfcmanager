/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Topaz.h
*
* Description: Topaz header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __TOPAZ_H__
#define __TOPAZ_H__

#define TOPAZ_HR_SIZE                  2     /**< %Topaz Header ROM size in bytes. */
#define TOPAZ_UID_SIZE                 4     /**< %Topaz Unique Identification (UID) number size in bytes. */

#define TOPAZ_TAG_SIZE                 120   /**< %Topaz total number of bytes (includes UID, Data, Reserved and Locks) on the tag. */
#define TOPAZ_TAG_DATA_SIZE            96    /**< %Topaz total number of data bytes on the tag. */

#define TOPAZ_FIRST_BLOCK              0     /**< %Topaz first available block number. */
#define TOPAZ_LAST_BLOCK               14    /**< %Topaz last available block number. */
#define TOPAZ_NUM_BLOCKS               15    /**< %Topaz total number of blocks. */

#define TOPAZ_FIRST_DATA_BLOCK         1     /**< %Topaz first available data block number. */
#define TOPAZ_LAST_DATA_BLOCK          12    /**< %Topaz last available data block number. */
#define TOPAZ_NUM_DATA_BLOCKS          12    /**< %Topaz total number of data blocks. */

#define TOPAZ_FIRST_BYTE               0     /**< %Topaz first available byte number. */
#define TOPAZ_LAST_BYTE                7     /**< %Topaz last available byte number. */

#define TOPAZ_BYTE_SIZE                1     /**< %Topaz byte size in bytes. */
#define TOPAZ_BLOCK_SIZE               8     /**< %Topaz block size in bytes. */

class wTopaz;

//! %Topaz IRT5011W.
/**
 * This class is designed to read and write to %Topaz IRT5011W tags. Care must
 * be taken when writing data to these tags because writing data can possibly
 * cause the tag to be no longer accessible. It is important to read the tag
 * manufacturer documentation first to better understanding the features and
 * functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * %Topaz tags are divided into 15 blocks with each block containing 8 bytes
 * of data. 7 byte are reserved for the Unique Identification (UID) number. 2
 * bytes are used for the read-only locking mechanism. 6 bytes are available
 * as One Time Programmable (OTP). 96 bytes are available as user programmable
 * read / write memory.\n
 * \n
 * Below is a diagram of the %Topaz (120 X 8 bit) EEPROM organized in 15
 * blocks of 8 bytes each.\n
 * \n
 * <img src="TopazMemory.jpg">
 * \n
 */
class Topaz
{
public:

   /*! \cond */
   Topaz();

   ~Topaz();
   /*! \endcond */

   /**
    * Locates a %Topaz tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a %Topaz tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a %Topaz tag currently
    *            in the iCarte field. This buffer must have a minimum size
    *            of #TOPAZ_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid);

   /**
    * Gets the metal-mask Header ROM of the last %Topaz tag that was located by a
    * call to IsTagAvailable().
    *
    * \param hr is a buffer used for storing the Header ROM of the last %Topaz
    *           tag that was located using IsTagAvailable(). This buffer must
    *           have a minimum size of #TOPAZ_HR_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetHR(unsigned char *hr);

   /**
    * Gets the Unique Identification (UID) number of the last %Topaz tag that was
    * located by a call to IsTagAvailable(). This method differs from ReadUID()
    * because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last %Topaz tag that
    *            was located using IsTagAvailable(). This buffer must
    *            have a minimum size of #TOPAZ_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number of the %Topaz tag currently
    * in the iCarte field.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #TOPAZ_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Reads a single byte from the specified byte address (Block number and Byte
    * number) of the %Topaz tag currently in the iCarte field.
    *
    * \param block is the desired block to access. Valid \a block number values are
    *              from #TOPAZ_FIRST_BLOCK to #TOPAZ_LAST_BLOCK.
    *
    * \param byte is the desired byte number to read from. Valid \a byte number
    *             values are from #TOPAZ_FIRST_BYTE to #TOPAZ_LAST_BYTE.
    *
    * \param readByte is a buffer used for storing the data read from the tag.
    *                 This buffer must have a minimum size of #TOPAZ_BYTE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadByte(unsigned char block, unsigned char byte, unsigned char *readByte);

   /**
    * Reads all blocks of the %Topaz tag currently in the iCarte field. Data from
    * block 0 to block 14 are read for a total of 120 bytes (UID0, UID1 ... OTP5).
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #TOPAZ_TAG_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadBlocks(unsigned char *readBuffer);

   /**
    * Writes a single byte to the specified byte address (Block number and Byte
    * number) of the %Topaz tag currently in the iCarte field. This method will
    * erase the contents of the byte address before writing the new data.
    *
    * \param block is the desired block to access. Valid \a block number values are
    *              from #TOPAZ_FIRST_DATA_BLOCK to #TOPAZ_LAST_DATA_BLOCK.
    *
    * \param byte is the desired byte number to write to. Valid \a byte number
    *             values are from #TOPAZ_FIRST_BYTE to #TOPAZ_LAST_BYTE.
    *
    * \param writeByte is a buffer used for storing the data to write to the tag.
    *                  This buffer must have a minimum size of #TOPAZ_BYTE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteByte(unsigned char block, unsigned char byte, unsigned char *writeByte);

   /**
    * Writes a single byte to the specified byte address (Block number and Byte
    * number) of the %Topaz tag currently in the iCarte field. This method will
    * not erase the contents of the byte address before writing the new data,
    * and the execution time is approximately half that of the WriteByte()
    * method. Bits can be set but not reset (i.e. bits previously set to a '1'
    * cannot be reset to a '0' using this method).
    *
    * \param block is the desired block to access. Valid \a block number values are
    *              from #TOPAZ_FIRST_DATA_BLOCK to #TOPAZ_LAST_DATA_BLOCK.
    *
    * \param byte is the desired byte number to write to. Valid \a byte number
    *             values are from #TOPAZ_FIRST_BYTE to #TOPAZ_LAST_BYTE.
    *
    * \param writeByte is a buffer used for storing the data to write to the tag.
    *                   This buffer must have a minimum size of #TOPAZ_BYTE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteByteNoErase(unsigned char block, unsigned char byte, unsigned char *writeByte);

private:

   /*! \cond */
   wTopaz  *wtopaz;
   /*! \endcond */
};

#endif
