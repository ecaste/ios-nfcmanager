/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Kovio.h
*
* Description: Kovio header file.
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __KOVIO_H__
#define __KOVIO_H__

#define KOV_UID_SIZE                   7     /**< %Kovio Unique Identification (UID) number size in bytes. */

#define KOV_PAGE_SIZE                  4     /**< %Kovio page size in bytes. */
#define KOV_PAGES_SIZE                 16    /**< %Kovio pages size in bytes (4 pages). */

#define KOV_TAG_SIZE                   256   /**< %Kovio total number of bytes (including UID, Internal, Locks, OTP and data) on the tag. */
#define KOV_TAG_DATA_SIZE              232   /**< %Kovio total number of data bytes on the tag. */

#define KOV_FIRST_PAGE                 0     /**< %Kovio first available page. */
#define KOV_LAST_PAGE                  63    /**< %Kovio last available page. */
#define KOV_NUM_PAGES                  64    /**< %Kovio total number of available pages. */

#define KOV_FIRST_DATA_PAGE            4     /**< %Kovio fisrt available data page. */
#define KOV_LAST_DATA_PAGE             61    /**< %Kovio last available data page. */
#define KOV_NUM_DATA_PAGES             58    /**< %Kovio total number of available data pages. */

#define KOV_LOCK_0_1_PAGE              2     /**< %Kovio Lock0 and Lock1 page. */
#define KOV_LOCK_2_3_4_5_PAGE          62    /**< %Kovio Lock2, Lock3, Lock4 and Lock5 page. */
#define KOV_LOCK_6_7_PAGE              63    /**< %Kovio Lock6 and Lock7 page. */

#define KOV_OTP_PAGE                   3     /**< %Kovio One Time Programmable page. */

class wKovio;

//! %Kovio K14T3N.
/**
 * This class is designed to read and write to %Kovio K14T3N tags. Care
 * must be taken when writing data to these tags because writing data is
 * one-time-programmable. Once a bit is programmed to a "1" it cannot
 * be changed back to a "0." It is important to read the tag
 * manufacturer documentation first to better understanding the
 * features and functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * %Kovio tags are divided into 64 pages with each page containing 4
 * bytes of data. 12 bytes are reserved for manufacturer data. 8 bytes are used
 * for the read-only locking mechanism. 4 bytes are available as One Time
 * Programmable (OTP). 232 bytes are available as user programmable read / write
 * memory.\n
 * \n
 * Below is a diagram of the %Kovio (2048 bit) EEPROM organized in 64
 * pages of 4 bytes each.\n
 * \n
 * <img src="KovioMemory.jpg">
 * \n
 */
class Kovio
{
public:

   /*! \cond */
   Kovio();

   ~Kovio();
   /*! \endcond */

   /**
    * Locates a %Kovio tag in the iCarte field. This method must be called
    * first before calling any other methods in this class which access the
    * tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a %Kovio tag in the iCarte field. This method must be called
    * first before calling any other methods in this class which access the
    * tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a %Kovio tag
    *            currently in the iCarte field. This buffer must have a minimum
    *            size of #KOV_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid);

   /**
    * Gets the Unique Identification (UID) number of the last %Kovio tag that
    * was located by a call to IsTagAvailable(). This method differs from
    * ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last %Kovio tag that
    *            was located using IsTagAvailable(). This buffer must have a
    *            minimum size of #KOV_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number of the %Kovio tag currently
    * in the iCarte field.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #KOV_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Reads data from the specified page of the %Kovio tag currently in the iCarte
    * field.
    *
    * \param page is the desired page to read from. Valid \a page values are from
    *             #KOV_FIRST_PAGE to #KOV_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #KOV_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPage(int page, unsigned char *readBuffer);

   /**
    * Reads 4 pages of data starting from the specified page of the %Kovio tag
    * currently in the iCarte field. A roll back is implemented; e.g. if page
    * 62 is specified, the contents of pages 62, 63, 0 and 1 are returned.
    *
    * \param page is the desired starting page to read from. Valid \a page values
    *             are from #KOV_FIRST_PAGE to #KOV_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #KOV_PAGES_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPages(int page, unsigned char *readBuffer);

   /**
    * Writes data to the specified page of the %Kovio tag currently in the iCarte
    * field. Data can only be written to user pages that are not locked. Warning!
    * If a bit is already programmed to a "1," then it cannot be changed back to
    * a "0."
    *
    * \param page is the desired page to write to. Valid \a page values are from
    *             #KOV_LOCK_0_1_PAGE to #KOV_LAST_PAGE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    tag. The data is bit-wise "or-ed" with the current contents
    *                    of \a page and the result is the new content. This buffer
    *                    must have a minimum size of #KOV_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WritePage(int page, unsigned char *writeBuffer);

private:

   /*! \cond */
   wKovio *wkovio;
   /*! \endcond */
};

#endif
