/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        SecureElement.h
*
* Description: Secure Element header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __SECUREELEMENT_H__
#define __SECUREELEMENT_H__

class wSecureElement;

//! Secure Element class.
/**
 * This class is designed to communicate with the iCarte Secure Element. The
 * Secure Element is embedded inside the iCarte and is running a Java Card Open
 * Platform operating system called JCOP31 v2.3.2 which is Java Card 2.2.1 and
 * Global Platform 2.1.1 compliant. The iCarte Secure Element has two interfaces:
 * contact interface with T=0 and T=1 protocols according to ISO/IEC 7816-3 and
 * contactless interface with T=CL protocol according to ISO/IEC 14443A-4.
 * To communicate with the iCarte Secure Element using the contact interface use
 * this class. To communicate with the iCarte Secure Element using the contactless
 * interface use the Utilities::SetContactless() method to enable
 * the iCarte Secure Element contactless interface then use the ISO14443A_4 class
 * or an external reader (including another iCarte) to communicate with the iCarte
 * Secure Element. For security reasons it is always a good idea to disable to
 * iCarte Secure Element contactless interface when not in use by using the
 * Utilities::SetContactless() method.\n
 * \n
 * The iCarte Secure Element can also emulate a Mifare Classic 1K tag. To enable
 * the Mifare Emulator use the Utilities::SetContactless() method.
 * When the Mifare Emulator is enabled the iCarte acts like a Mifare Classic
 * 1K tag and can be accessed by any external reader (including another iCarte) that
 * is Mifare Classic 1K compliant. The Mifare Emulator can also be accessed using
 * the contactless interface using the Mifare1KEmulator class. For security reasons
 * it is always a good idea to disable the iCarte Secure Element contactless interface
 * when not in use by using the Utilities::SetContactless() method.\n
 * \n
 * Please refer to the ISO/IEC 7816-4, ISO/IEC 14443A-4, Java Card Open Platform
 * and Global Platform documentation for more information.\n
 */
class SecureElement
{
public:

   /*! \cond */
   SecureElement();

   ~SecureElement();
   /*! \endcond */

   /**
    * Activates the iCarte Secure Element. A cold reset or warm reset is initiated
    * depending on the state of the Secure Element. The Answer to Reset is returned
    * in response to the reset. Call GetATR() to retrieve the ATR.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Activate();

   /**
    * Deactivates the iCarte Secure Element. After the iCarte Secure Element has been
    * deactivated communication is no longer possible until Activate() is called.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Deactivate();

   /**
    * Gets the Answer to Reset (ATR) from the iCarte Secure Element.
    *
    * \param atr is a buffer used for storing the ATR. This buffer must have a
    *            minimum size of \a atrLength. Please refer to the ISO/IEC 7816-3
    *            specification for more information on how to decode this data.
    *
    * \param atrLength for input the length of \a atr, for output the
    *                  number of bytes received from the iCarte Secure Element.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetATR(unsigned char *atr, int *atrLength);

   /**
    * Sends a command APDU to the iCarte Secure Element. This method does not
    * attempt to verify that the command APDU is semantically correct. There
    * shall be no interleaving of command-response pairs across the interface,
    * i.e. the response APDU shall be received before initiating another
    * command APDU. Extended length command APDUs are not supported.
    *
    * \param writeBuffer is a buffer used for storing the data to send to the
    *                    Secure Element. The data must be in command APDU format
    *                    according to the ISO/IEC 7816-4 specification.
    *
    * \param writeLength is the number of bytes to send to the Secure Element.
    *                    Do not exceed the maximum APDU size. If your data
    *                    exceeds the maximum APDU size you must chain your data.
    *                    Please refer to the ISO/IEC 7816-3 specification for
    *                    more information on how to chain your data.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SendCommand(unsigned char *writeBuffer, int writeLength);

   /**
    * Gets a response APDU from the iCarte Secure Element. The \a readBuffer
    * contains the actual received data in response to a command APDU. This
    * method does not attempt to verify that the response APDU is semantically
    * correct. Be sure to set \a readLength to the size of \a readBuffer before
    * calling this method.
    *
    * \param readBuffer is a buffer used for storing the response. This buffer
    *                   must be at least \a readLength bytes in length.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received from the Secure Element.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetResponse(unsigned char *readBuffer, int *readLength);

private:

   /*! \cond */
   wSecureElement *wsecureelement;
   /*! \endcond */
};

#endif
