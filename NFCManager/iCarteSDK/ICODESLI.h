/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        ICODESLI.h
*
* Description: I-CODE SLI header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __ICODESLI_H__
#define __ICODESLI_H__

#define ICODESLI_UID_SIZE                    8     /**< I-CODE SLI Unique Identification (UID) number size in bytes. */
#define ICODESLI_BLOCK_SIZE                  4     /**< I-CODE SLI User Data block size in bytes. */
#define ICODESLI_BLOCK_SECURITY_STATUS_SIZE  1     /**< I-CODE SLI Block Security Status size in bytes. */

#define ICODESLI_FIRST_BLOCK                 0     /**< I-CODE SLI first User Data block address. */
#define ICODESLI_LAST_BLOCK                  27    /**< I-CODE SLI last User Data block address. */
#define ICODESLI_NUM_BLOCKS                  28    /**< I-CODE SLI total number of User Data blocks. */

#define ICODESLI_EAS_SEQUENCE_SIZE           32    /**< I-CODE SLI Electronic Article Surveillance (EAS) sequence size in bytes. */

class wICODESLI;

//! I-CODE SLI SL2ICS20.
/**
 * This class is designed to read and write to %NXP I-CODE SLI SL2ICS20 tags.
 * The I-CODE SLI is a tag used for intelligent label applications like supply
 * chain management as well as baggage and parcel identification in airline business
 * and mail services. This tag is the first member of a product family of smart
 * label ICs based on the ISO standard ISO/IEC 15693-3. The I-CODE SLI tag offers
 * the ability of operating multiple tags simultaneously in the iCarte field. Care
 * must be taken when writing data to these tags because writing data can possibly
 * cause the tag to be no longer accessible. It is important to read the tag
 * manufacturer documentation first to better understanding the features and
 * functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * I-CODE SLI tags are divided into 32 blocks with each block containing 4 bytes
 * of data. The higher 28 blocks contain user data and the lowest 4 blocks
 * contain the Unique Identification (UID) number, the write access conditions
 * and special data like AFI and DSFID.\n
 * \n
 * Below is a diagram of the I-CODE SLI (1024 bit) EEPROM organized in 32
 * blocks of 4 bytes each.\n
 * \n
 * <img src="ICODESLIMemory.jpg">
 * \n
 */
class ICODESLI
{
public:

   /*! \cond */
   ICODESLI();

   ~ICODESLI();
   /*! \endcond */

   /**
    * Locates an I-CODE SLI tag in the iCarte field. This method must
    * be called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag. After a
    * tag is located it is automatically selected.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates an I-CODE SLI tag with the specified Application
    * Family Identifier (AFI) in the iCarte field. This method must be called
    * first before calling any other methods in this class which access the
    * tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag. After a
    * tag is located it is automatically selected.
    *
    * \param afi is the Application Family Identifier (AFI). Only tags with a
    *            matching AFI will respond to this method. For more information
    *            on the AFI please refer to the ISO/IEC 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char afi);

   /**
    * Gets the Unique Identification (UID) number of the last I-CODE SLI tag
    * that was located by a call to IsTagAvailable(). This method differs from
    * ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ICODESLI_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number from the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable().
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ICODESLI_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Gets the tag information of the last I-CODE SLI tag that was located by
    * a call to IsTagAvailable(). This method differs from ReadTagInfo() because
    * it doesn't read any data from the tag.
    *
    * \param tagInfo is the reference used for storing the tag information.
    *                See #ISO15693TagInfo for more information on its parameters.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetTagInfo(ISO15693TagInfo &tagInfo);

   /**
    * Reads the tag information from the last I-CODE SLI tag that was located
    * by a call to IsTagAvailable().
    *
    * \param tagInfo is the reference used for storing the tag information.
    *                See #ISO15693TagInfo for information on its parameters.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadTagInfo(ISO15693TagInfo &tagInfo);

   /**
    * Reads data from the specified User Data block from the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable().
    *
    * \param block is the desired block to read from. Valid \a block values are
    *              from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param readBuffer is a buffer used for storing the Block Security Status
    *                   and or the User Block data. This buffer must have a
    *                   minimum size of #ICODESLI_BLOCK_SIZE. If the
    *                   #ISO15693Parameters::optionFlag is set you must increase the
    *                   buffer size by #ICODESLI_BLOCK_SECURITY_STATUS_SIZE so
    *                   that the Block Security Status can be inserted as the
    *                   first byte in \a readBuffer.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadBlock(int block, unsigned char *readBuffer, int *readLength);

   /**
    * Reads data from multiple User Data blocks from the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable().
    *
    * \param firstBlock is the first block to read from. Valid \a block values are
    *                   from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param numberOfBlocks is the number of blocks to read. Valid values are
    *                       from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param readBuffer is a buffer used for storing the Block Security Status
    *                   and or the User Block data. This buffer must have a
    *                   minimum size of #ICODESLI_BLOCK_SIZE * \a numberOfBlocks.
    *                   If the #ISO15693Parameters::optionFlag is set
    *                   you must increase the buffer size by \a numberOfBlocks
    *                   so that the Block Security Status can be inserted before
    *                   each User Block data. The Block Security Status followed
    *                   by the User Block data will be repeated \a numberOfBlocks
    *                   for each block.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadMultipleBlocks(int firstBlock, int numberOfBlocks, unsigned char *readBuffer, int *readLength);

   /**
    * Writes data to the specified User Data block of the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable().
    *
    * \param block is the desired block to write to. Valid \a block values are
    *              from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param writeBuffer is the  buffer used for storing the User Block data
    *                    to write to the tag. This buffer must have a minimum
    *                    size of #ICODESLI_NUM_BLOCKS.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteBlock(int block, unsigned char *writeBuffer);

   /**
    * Locks the specified User Data block of the last I-CODE_SLI tag that was
    * located by a call to IsTagAvailable(). It is important to note that when a
    * block on the tag has been locked it can never be written to again. It is
    * not possible to unlock a block once it has been locked.
    *
    * \param block is the desired block to lock. Valid \a block values are from
    *              #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockBlock(int block);

   /**
    * Writes the Data Storage Format Identifier (DSFID) to the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). For more information
    * about the DSFID please refer to the ISO/IEC 15693-3 specification.
    *
    * \param dsfid is the DSFID to write to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteDSFID(unsigned char dsfid);

   /**
    * Locks the Data Storage Format Identifier (DSFID) of the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). It is important to
    * note that when the DSFID on the tag has been locked it can never be
    * written to again. It is not possible to unlock the DSFID once it has been
    * locked. Make necessary changes to the DSFID using WriteDSFID() before
    * calling this method. For more information on the DSFID please refer to the
    * ISO/IEC 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockDSFID();

   /**
    * Writes the Application Family Identifier (AFI) to the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). For more information
    * about the AFI please refer to the ISO/IEC 15693-3 specification.
    *
    * \param afi is the AFI to write to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteAFI(unsigned char afi);

   /**
    * Locks the Application Family Identifier (AFI) of the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). It is important to
    * note that when the AFI on the tag has been locked it can never be written
    * to again. It is not possible to unlock the AFI once it has been locked.
    * Make necessary changes to the AFI using WriteAFI() before calling this
    * method. For more information on the AFI please refer to the ISO/IEC
    * 15693-3 specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_INCORRECT_SETTING
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockAFI();

   /**
    * Reads the Electronic Article Surveillance (EAS) sequence from the last
    * I-CODE SLI tag that was located by a call to IsTagAvailable(). If the tag
    * has its EAS bit set to 1 \a readBuffer will contain the EAS sequence. If
    * the tag has its EAS bit set to 0 the tag remains silent. For more
    * information on the EAS please refer to the I-CODE SLI functional
    * specification.
    *
    * \param readBuffer is a buffer used for storing EAS sequence read from the
    *                   tag. This buffer must have a minimum size of
    *                   #ICODESLI_EAS_SEQUENCE_SIZE.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.

    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    * \retval #ERR_ICODESLI_INCORRECT_EAS_LENGTH
    * \retval #ERR_ICODESLI_INCORRECT_EAS_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadEAS(unsigned char *readBuffer, int *readLength);

   /**
    * Writes the Electronic Article Surveillance (EAS) bit to the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). For more information
    * on the EAS please refer to the I-CODE SLI functional specification.
    *
    * \param eas is the value to set or reset the EAS bit. Setting \a eas to
    *            \a true will enable EAS and setting \a eas to \a false
    *            will disable EAS.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WriteEAS(bool eas);

   /**
    * Locks the Electronic Article Surveillance bit of the last I-CODE SLI
    * tag that was located by a call to IsTagAvailable(). It is important to
    * note that when the EAS on the tag has been locked it can never be written
    * to again. It is not possible to unlock the EAS once locked. Make necessary
    * changes to the EAS using WriteEAS() before calling this method. For more
    * information on the EAS please refer to the I-CODE SLI functional
    * specification.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR LockEAS();

   /**
    * Reads multiple User Data locks from the last I-CODE SLI tag that was
    * located by a call to IsTagAvailable().
    *
    * \param firstBlock is the first block to read from. Valid \a block values are
    *                   from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param numberOfBlocks is the number of blocks to read. Valid values are
    *                       from 0 to #ICODESLI_NUM_BLOCKS
    *
    * \param readBuffer is a buffer used to store the lock data. This buffer
    *                   must have a minimum size of #ICODESLI_NUM_BLOCKS. Each byte
    *                   in \a readBuffer corresponds to a block on the tag. If
    *                   the corresponding byte is 0x00 then the block is unlocked.
    *                   All other values denote a locked block.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadLocks(int firstBlock, int numberOfBlocks, unsigned char *readBuffer, int *readLength);

   /**
    * Writes multiple User Data locks to the last I-CODE SLI tag that was
    * located by a call to IsTagAvailable(). It is important to note that when a
    * block on the tag has been locked it can never be written to again. It is
    * not possible to unlock a block once it has been locked.
    *
    * \param firstBlock is the first block to lock. Valid \a block values are
    *                   from #ICODESLI_FIRST_BLOCK to #ICODESLI_LAST_BLOCK.
    *
    * \param numberOfBlocks is the number of blocks to lock. Valid values are
    *                       from 0 to #ICODESLI_NUM_BLOCKS.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO15693_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    *
    * \note Changing the locks must be done in a safe environment. The tag must
    *       not be moved out of the iCarte field during writing! We recommend
    *       putting the tag close to the iCarte and not move it during locking.
    */
   ERR WriteLocks(int firstBlock, int numberOfBlocks);

   /**
    * Sets the ICODESLI class parameters. These parameters must be set in
    * order to properly communicate with an I-CODE SLI tag. See
    * #ISO15693Parameters for more information on its parameters.
    *
    * \param parameters is the new parameters that will be set.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetParameters(ISO15693Parameters parameters);

   /**
    * Gets the current I-CODE SLI class parameters.
    *
    * \param parameters is the reference used for storing the current I-CODE SLI
    *                   parameters
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetParameters(ISO15693Parameters &parameters);

private:

   /*! \cond */
   wICODESLI *wicodesli;
   /*! \endcond */
};

#endif
