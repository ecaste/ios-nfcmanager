/******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Errorcodes.h
*
* Description: Error codes header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
******************************************************************************/
#ifndef __ERRORCODES_H__
#define __ERRORCODES_H__


/******************************************************************************
* Typdefs
******************************************************************************/
typedef long ERR;


/******************************************************************************
* General Errors - 100000
******************************************************************************/

//! No Error
#define ERR_NONE                                    0

//! Tag Available
#define ERR_TAG_AVAILABLE                           1

//! API Error
/*! An unexpected error in the API occurred.
 *  Please note the details which caused this
 *  error and contact support for more
 *  information.
 */
#define ERR_GEN_API_ERROR                           100000

//! API Feature Not Implemented
/*! The feature you are using has not yet been
 *  implemented.
 */
#define ERR_GEN_API_FEATURE_NOT_IMPLEMENTED         100001

//! API Upgrade Required
/*! You need to upgrade your API libraries in
 *  order to use this feature.
 */
#define ERR_GEN_API_UPGRADE_REQUIRED                100005

//! Firmware Upgrade Required
/*! You need to upgrade your iCarte in order
 *  to use this feature.
 */
#define ERR_GEN_FW_UPGRADE_REQUIRED                 100010

//! Null Pointer
/*! A null pointer was passed to this method.
 *  Hold out your hand and slap it!
 */
#define ERR_GEN_NULL_POINTER                        100100

//! Buffer Overrun
/*! The buffer you have provided is not large
 *  enough to hold the data.
 */
#define ERR_GEN_BUFFER_OVERRUN                      100101

//! Buffer Underrun
/*! The buffer you have provided does not
 *  contain enough data.
 */
#define ERR_GEN_BUFFER_UNDERRUN                     100102

//! Out of Range
/*! The value you have provide is out of range.
 *  Please check the documentation for the
 *  an appropriate value and retry.
 */
#define ERR_GEN_OUT_OF_RANGE                        100110

//! Incorrect Setting
/*! The parameter settings or class settings
 *  are not valid. Please change your settings
 *  and retry.
 */
#define ERR_GEN_INCORRECT_SETTING                   100115

//! Incorrect Command
/*! The command is not in the correct format.
 */
#define ERR_GEN_INCORRECT_COMMAND                   100120

//! Incomplete Command
/*! The iCarte did not receive enough data
 *  to perform operation. Please check the
 *  command to see if it is formatted correctly
 *  and try again.
 */
#define ERR_GEN_INCOMPLETE_COMMAND                  100121

//! Incorrect Response
/*! The response is not in the correct format.
 */
#define ERR_GEN_INCORRECT_RESPONSE                  100130

//! Incomplete Response
/*! The iCarte has received an incomplete
 *  response.
 */
#define ERR_GEN_INCOMPLETE_RESPONSE                 100131

//! Checksum Error
/*! The iCarte has detected a checksum error.
 */
#define ERR_GEN_CS_ERROR                            100140

//! CRC Error
/*! The iCarte has detected a CRC error.
 */
#define ERR_GEN_CRC_ERROR                           100141

//! Parity Error
/*! The iCarte has detected a parity error.
 */
#define ERR_GEN_PARITY_ERROR                        100142

//! Collision Error
/*! The iCarte has detected a collision error.
 */
#define ERR_GEN_COLLISION_ERROR                     100145

//! Timeout
/*! The action has timed out. Is the iCarte
 *  still attached?
 */
#define ERR_GEN_TIMEOUT                             100199

//! No Response
/*! No response was received. Was the tag
 *  taken out of field? Is the tag locked?
 *  Was the command formatted correctly?
 */
#define ERR_GEN_NO_RESPONSE                         100200

//! Memory Error
/*! An error occurred when trying to create
 *  memory. Trying cleaning up some memory.
 */
#define ERR_GEN_MEMORY_ERROR                        100998

//! Unknown Error
/*! An unknown error occurred. Please note
 *  the details which caused this error and
 *  contact support for more information.
 */
#define ERR_GEN_UNKNOWN_ERROR                       100999


/******************************************************************************
* iCarte Errors - 101000 block
******************************************************************************/

//! iCarte Not Connected
/*! The iCarte is not connected. Attach iCarte
 *  and try again. You should call
 *  Utilities::IsiCarteConnected() first and wait
 *  for it to return TRUE before calling any other
 *  API method. You may also receive this error
 *  if Utilities::SetPower() has been called with
 *  #UTIL_POWER_MODE_OFF.
 */
#define ERR_ICARTE_NOT_CONNECTED                    101000

//! Connection Error
/*! The iCarte has encountered a connection
 *  error and may have been removed. Attach
 *  iCarte and try again.
 */
#define ERR_ICARTE_CONNECTION_ERROR                 101001

//! Stream Error
/*! The API has encountered a stream error
 *  while trying to either send or receive
 *  data to or from the iCarte.
 */
#define ERR_ICARTE_STREAM_ERROR                     101002

//! Initialization Error
/*! The iCarte has encountered an error while
 *  trying to initialize one of its components.
 *  It may be necessary to remove the iCarte
 *  and reattach it. If the problem pursists
 *  please contact support for more information.
 */
#define ERR_ICARTE_INITIALIZATION_ERROR             101005

//! Authentication Error
/*! The iCarte has encountered an error while
 *  trying to authenticate. The iCarte is
 *  probably locked. See Utilities::Authenticate()
 *  for more information.
 */
#define ERR_ICARTE_AUTHENTICATION_ERROR             101010

//! Incorrect Header Length
/*! The header received has an unexpected
 *  length. Although the iCarte will attempt
 *  to resync check your code and make sure
 *  you are not making an API call while
 *  waiting for another API call to return.
 */
#define ERR_ICARTE_INCORRECT_HEADER_LENGTH          101020

//! Incorrect Response Length
/*! The response received has an unexpected
 *  length. Although the iCarte will attempt
 *  to resync check your code and make sure
 *  you are not making an API call while
 *  waiting for another API call to return.
 */
#define ERR_ICARTE_INCORRECT_RESPONSE_LENGTH        101021

//! Incorrect Response
/*! The response received is erronious.
 *  Although the iCarte will attempt
 *  to resync check your code and make sure
 *  you are not making an API call while
 *  waiting for another API call to return.
 */
#define ERR_ICARTE_INCORRECT_RESPONSE               101022

//! iCarte Transport Too Much Data
/*! The iCarte has too much data to transfer.
 */
#define ERR_ICARTE_TOO_MUCH_DATA                    101023

//! Retrieval Of Apps UUID Failed
/*! Could not retrieve applications UUID
 *  from the keychain.
 */
#define ERR_ICARTE_RETRIEVE_UUID_FAILED             101024

//! Unknown Error
/*! An unknown error occurred. Please note
 *  the details which caused this error and
 *  contact support for more information.
 */
#define ERR_ICARTE_UNKNOWN_ERROR                    101099

/******************************************************************************
* Security Errors - 110000 block
******************************************************************************/

//! Security Elements Are Disabled
/*! The iCarte security elements are
 *  currently disabled. Get Authentication
 *  status to retrieve list of enabled/disabled.
 *  elements.
 */
#define ERR_SECURITY_DISABLED                       110000

//! iCarte Not Activated
/*! The iCarte has not been activated yet.
 */
#define ERR_SECURITY_ICARTE_NOT_ACTIVATED           110001

//! iCarte Locked
/*! The iCarte is locked. Use
 *  Utilities::Authenticate() to unlocked iCarte.
 */
#define ERR_SECURITY_ICARTE_LOCKED                  110002


/******************************************************************************
* Crosscheck Errors - 120000 block
******************************************************************************/

//! Not Authenticated
/*! The attempted command failed
 *  because the iCarte is not authenticated.
 *  Authenticate using Crosscheck.Authenticate
 */
#define ERR_CROSSCHECK_NOT_AUTHENTICATED            120000

//! Incorrect Password
/*! The Password supplied is incorrect. Please
 *  authenticate using Crosscheck.Authenticate
 *  with the correct Password.
 *  NOTE: If the
 *  CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT
 *  feature is enabled, you should be careful
 *  not to send the password incorrectly as you
 *  may eventually lockout the iCarte.
 */
#define ERR_CROSSCHECK_INCORRECT_PASSWORD           120001

//! Already Authenticated
/*! The attempted iCarte Authentication failed
 *  because the iCarte is already authenticated.
 */
#define ERR_CROSSCHECK_ALREADY_AUTHENTICATED        120002

//! Locked
/*! The iCarte is still locked.
 *  Need to use Utilities.Authenticate method
 *  to correctly unlock iCarte.
 */
#define ERR_CROSSCHECK_LOCKED                       120003

//! UnLocked
/*! iCarte has been unlocked.
 */
#define ERR_CROSSCHECK_UNLOCKED                     120004

//! Authentication Error
/*! iCarte failed to authenticate.
 */
#define ERR_CROSSCHECK_AUTH_ERROR                   120005

//! Incorrect Argument
/*! The Token was not used correctly with the
 *  correct argument, refer to the documentation
 *  for the TLV you are trying to get/set to
 *  ensure compliance.
 */
#define ERR_CROSSCHECK_INCORRECT_ARGUMENT           120006

//! Incorrect Token
/*! The token specified is not suitable
 *  for the get/set method or is unknown.
 */
#define ERR_CROSSCHECK_INCORRECT_TOKEN              120007

//! Incorrect Token Argument
/*! The token specified is not suitable
 *  for the get/set method or is unknown.
 */
#define ERR_CROSSCHECK_INCORRECT_TOKEN_ARGUMENT     120008

//! Incorrect Token State
/*! Either the token does not requires a
 *  CROSSCHECK_ELEMENT_STATE
 *  and should be set to
 *  CROSSCHECK_ELEMENT_NOT_APPLICABLE or
 *  the token does require a
 *  CROSSCHECK_ELEMENT_STATE
 *  and should be set to CROSSCHECK_ELEMENT_ENABLE
 *  or CROSSCHECK_ELEMENT_DISABLE.
 */
#define ERR_CROSSCHECK_INCORRECT_TOKEN_STATE        120009

//! Token Value Length Error
/*! The length of the value supplied for the
 *  token is not in the ideal range. Refer to
 *  the documentation for the specified token
 *  to ensure the correct length is used.
 */
#define ERR_CROSSCHECK_TOKEN_VALUE_LENGTH           120010

//! Carrier Not Available
/*! The Carrier Name could not be retrieve either
 *  because iPhone is in Airplane mode, there is
 *  no SIM inserted, or no Carrier signal is
 *  obtainable.
 */
#define ERR_CROSSCHECK_CARRIER_NOT_AVAILABLE        120011

//! Permanently Lockout
/*! The iCarte has been permanently locked out,
 *  need to reconfigure the Crosscheck with
 *  correct password.
 */
#define ERR_CROSSCHECK_PERMANENT_LOCKOUT            120012

//! Element Missing
/*! Crosscheck element is missing from the iCarte.
 *  Use the Crosscheck class to configure and/or
 *  configure crosscheck elements.
 */
#define ERR_CROSSCHECK_ELEMENT_MISSING              120013

//! Unknown Error
/*! Please note the details which caused this
 *  error and contact support for more
 *  information.
 */
#define ERR_CROSSCHECK_UNKNOWN_ERROR                120099


/******************************************************************************
* ISO/IEC 14443 Errors - 140000 block
******************************************************************************/

//! REQA Failed
/*! The Request Command, type A, failed.
 */
#define ERR_ISO14443_REQA_FAILED                    140000

//! Incorrect ATS Response
/*! The Answer To Select Command response was
 *  incorrect.
 */
#define ERR_ISO14443_INCORRECT_ATS_RESPONSE         140012

//! Incorrect PPS Response
/*! The Protocol and Parameter Selection
 *  Command response was incorrect.
 */
#define ERR_ISO14443_INCORRECT_PPS_RESPONSE         140014

//! Incorrect PPSS Response
/*! The Protocol and Parameter Selection Start
 *  Command response was incorrect.
 */
#define ERR_ISO14443_INCORRECT_PPSS_RESPONSE        140016

//! REQB Failed
/*! The Request Command, type B, failed.
 */
#define ERR_ISO14443_REQB_FAILED                    140050

//! Incorrect SM Response
/*! The Slot-MARKER Command response was
 *  incorrect.
 */
#define ERR_ISO14443_INCORRECT_SM_RESPONSE          140055

//! Incorrect ATTRIB Response
/*! The PICC Selection Command response was
 *  incorrect.
 */
#define ERR_ISO14443_INCORRECT_ATTRIB_RESPONSE      140064

//! Anticollision Failed
/*! A collision between two or more tags in
 *  field was detected and the iCarte was
 *  unable to distinguish from which tag
 *  the data originated.
 */
#define ERR_ISO14443_ANTICOLLISION_FAILED           140070

//! Command Not Supported
/*! The specified command is not supported.
 *  Please check the ISO/IEC 14443
 *  documentation for more information.
 */
#define ERR_ISO14443_COMMAND_NOT_SUPPORTED          140080

//! Command Failed
/*! The specified command failed. Check the
 *  command format and try again.
 */
#define ERR_ISO14443_COMMAND_FAILED                 140081

//! Proprietary Protocol Detected
/*! A proprietary command or protocol was
 *  detected. Proceed with commands and
 *  protocols defined in the ISO/IEC 14443-4
 *  specification.
 */
#define ERR_ISO14443_PROPRIETARY_PROTOCOL_DETECTED  140090

//! No Tag Selected
/*! There is no tag currently in field or it
 *  is not selected. If a tag is in field try
 *  selecting it first.
 */
#define ERR_ISO14443_NO_TAG_SELECTED                140098

//! Unknown Error
/*! The tag returned an unknown error. Please
 *  note the details which caused this error
 *  and contact the tag manufacturer for more
 *  information.
 */
#define ERR_ISO14443_UNKNOWN_ERROR                  140099


/******************************************************************************
* Mifare Errors - 141000 block
******************************************************************************/

//! Authentication Failed
/*! You must athenticate the tag before you
 *  can access it. Check that the key is
 *  correct and you are using the correct key
 *  type.
 */
#define ERR_MIFARE_AUTHENTICATION_FAILED            141000

//! Command Failed
/*! The tag returned that the command failed
 *  due to an unknown reason.
 */
#define ERR_MIFARE_COMMAND_FAILED                   141001

//! Incomplete Response
/*! The tag returned an incomplete response.
 */
#define ERR_MIFARE_INCOMPLETE_RESPONSE              141002

//! NACK Underflow or Overflow
/*! The tag returned a non acknowledgement
 *  underflow or overflow response. This
 *  usually occurs after sending an increment
 *  or decrement command and the value being
 *  incremented or decremented either
 *  overflows or underflows.
 */
#define ERR_MIFARE_NACK_UNDERFLOW_OVERFLOW          141003

//! NACK Transmission Error
/*! The tag returned a non acknowledgement
 *  transmission error response.
 */
#define ERR_MIFARE_NACK_TRANSMISSION_ERROR          141004

//! Unknown NACK Response
/*! The tag returned an unknown non
 *  acknowledgement response.
 */
#define ERR_MIFARE_UNKNOWN_NACK_RESPONSE            141005


/******************************************************************************
* ISO/IEC 15693 Errors - 150000 block
******************************************************************************/

//! Command Not Supported
/*! The tag returned that the command is not
 *  supported, i.e. the request code is not
 *  recognized. Please check the ISO/IEC 15693
 *  documentation for more information.
 */
#define ERR_ISO15693_CMD_NOT_SUPPORTED              150001

//! Command Not Recognized
/*! The tag returned that the command is not
 *  recognized, i.e. a format error occurred.
 *  Please check the ISO/IEC 15693
 *  documentation for more information.
 */
#define ERR_ISO15693_CMD_NOT_RECOGNIZED             150002

//! Command Option Not Supported
/*! The tag returned that the command option
 *  is not supported. Try clearing the option
 *  flag and resend the command.
 */
#define ERR_ISO15693_CMD_OPT_NOT_SUPPORTED          150003

//! Unsupported Error
/*! The tag returned an error with no other
 *  information given or a specific error code
 *  is not supported. This may occur with some
 *  I-CODE SLI tags when reading blocks outside
 *  of the tags memory range. Certain tags will
 *  also go quiet if this is the case and will
 *  need to be removed from field and put back
 *  in to reactivate.
 */
#define ERR_ISO15693_UNSUPPORTED_ERROR              150015

//! Block Not Available
/*! The tag returned that the specified block
 *  is not available or doesn't exist.
 */
#define ERR_ISO15693_BLOCK_NOT_AVAILABLE            150016

//! Block Already Locked
/*! The tag returned that the specified block
 *  is already locked and thus cannot be
 *  locked again.
 */
#define ERR_ISO15693_BLOCK_ALREADY_LOCKED           150017

//! Block Locked
/*! The tag returned that the specified block
 *  is locked and its contents cannot be
 *  changed.
 */
#define ERR_ISO15693_BLOCK_LOCKED                   150018

//! Block Not Programmed
/*! The tag returned that the specified block
 *  was not successfully programmed. Make sure
 *  the tag is completely in the field and
 *  retry.
 */
#define ERR_ISO15693_BLOCK_NOT_PROGRAMMED           150019

//! Block Not Locked
/*! The tag returned that the specified lock
 *  was not successfully locked. Please retry.
 */
#define ERR_ISO15693_BLOCK_NOT_LOCKED               150020

//! Incorrect Response
/*! The response received from the tag was
 *  not correct. Please retry.
 */
#define ERR_ISO15693_INCORRECT_RESPONSE             150097

//! No Tag Selected
/*! There is no tag currently in field or it
 *  is not selected. If a tag is in field try
 *  selecting it first.
 */
#define ERR_ISO15693_NO_TAG_SELECTED                150098

//! Unknown Error
/*! The tag returned an unknown error. Please
 *  note the details which caused this error
 *  and contact the tag manufacturer for more
 *  information.
 */
#define ERR_ISO15693_UNKNOWN_ERROR                  150099


/******************************************************************************
* I-CODE SLI Errors - 151000 block
******************************************************************************/

//! Incorrect EAS Length
/*! The EAS sequenced recevied from the tag was
 *  not the correct length.
 */
#define ERR_ICODESLI_INCORRECT_EAS_LENGTH           151010

//! Incorrect EAS Response
/*! The EAS sequenced recevied from the tag was
 *  not as expected.
 */
#define ERR_ICODESLI_INCORRECT_EAS_RESPONSE         151020

//! Unknown Error
/*! The tag returned an unknown error. Please
 *  note the details which caused this error
 *  and contact the tag manufacturer for more
 *  information.
 */
#define ERR_ICODESLI_UNKNOWN_ERROR                  151099


/******************************************************************************
* Secure Element Errors - 800000 block
******************************************************************************/

//! Communication Failure
/*! The iCarte has detected a communication
 *  failure. This may occur if the Secure
 *  Element has not been activated yet.
 */
#define ERR_SE_COMMUNICATION_FAILURE                800000

//! Activate Error
/*! The Secure Element has either not been
 *  activated yet, or has failed to activate.
 *  Please retry.
 */
#define ERR_SE_ACTIVATE_ERROR                       800001

//! Incorrect ATR Length
/*! The Answer to Reset command received was
 *  either incorrect or cannot fit into the
 *  memory allocated.
 */
#define ERR_SE_INCORRECT_ATR_LENGTH                 800005

//! Transmission Mode Not Supported
/*! The transmission mode specified is not
 *  correct or is not supported.
 */
#define ERR_SE_TRANSMISSION_MODE_NOT_SUPPORTED      800010

//! Incorrect APDU
/*! The APDU instruction field is incorrect.
 */
#define ERR_SE_INCORRECT_APDU                       800020

//! Incorrect APDU Length
/*! Too much data was provided.
 */
#define ERR_SE_INCORRECT_APDU_LENGTH                800022

//! Chaining aborted
/*! ISO7816-3 T=1 Block chaining was aborted by
 *  either the iCarte or the secure element.
 *  This can be cause by incorrect buffer lengths.
 */
#define ERR_SE_CHAINING_ABORTED                     800023

//! Secure Element resynched.
/*! The secure element has been resyched after
 *  detecting a communication error. The resych
 *  may have reset communicaiton with the secure
 *  element.
 */
#define ERR_SE_RESYNCHED                            800024

//! Function Not Supported
/*! The requested function is not supported.
 */
#define ERR_SE_FUNCTION_NOT_SUPPORTED               800030

//! Memory Failure
/*! The Secure Element experienced a memory
 *  failure when trying to execute the command.
 */
#define ERR_SE_MEMORY_FAILURE                       800050

//! Not Enought Memory
/*! The Secure Element doesn't have enough
 *  memory to complete the current command or
 *  operation.
 */
#define ERR_SE_NOT_ENOUGH_MEMORY                    800051

//! Unexpected Error
/*! The Secure Element returned an unexpected
 *  status word. Check the APDU to see if it
 *  is semantically correct.
 */
#define ERR_SE_UNEXPECTED_ERROR                     800098

//! Unknown Error
/*! The Secure Element returned an unknown
 *  error. Please note the details which caused
 *  this error and contact the applet provider
 *  for more information.
 */
#define ERR_SE_UNKNOWN_ERROR                        800099


/******************************************************************************
* Utilities Errors - 900000 block
******************************************************************************/

//! No Field Event Detected
/*! This is a notification that the iCarte has
 *  not detected a field.
 */
#define ERR_UTIL_FIELD_DETECT_NO_EVENT              900000

//! Exit Field Event
/*! This is a notification that the iCarte has
 *  exited the field of another reader.
 */
#define ERR_UTIL_FIELD_DETECT_EVENT_EXIT            900001

//! Enter Field Event
/*! This is a notification that the iCarte has
 *  entered the field of another reader.
 */
#define ERR_UTIL_FIELD_DETECT_EVENT_ENTER           900002

//! iCarte Authenitcation Error
/*! iCarte failed to authenticate.
 */
#define ERR_UTIL_AUTH_ERROR                         900050

//! Already Authenticated
/*! The attempted iCarte Authentication failed
 * becasue the iCarte is already authenticated.
 */
#define ERR_UTIL_ALREADY_AUTHENTICATED              900051

//! Unknown Error
/*! Please note the details which caused this
 *  error and contact support for more
 *  information.
 */
#define ERR_UTIL_UNKNOWN_ERROR                      900099

#endif
