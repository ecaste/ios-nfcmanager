/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        MifareUL.h
*
* Description: Mifare Ultralight header file.
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __MIFAREUL_H__
#define __MIFAREUL_H__

#define MUL_UID_SIZE                   7     /**< Mifare Ultralight Unique Identification (UID) number size in bytes. */

#define MUL_PAGE_SIZE                  4     /**< Mifare Ultralight page size in bytes. */
#define MUL_PAGES_SIZE                 16    /**< Mifare Ultralight pages size in bytes (4 pages). */

#define MUL_TAG_SIZE                   64    /**< Mifare Ultralight total number of bytes (including UID, Internal, Locks, OTP and data) on the tag. */
#define MUL_TAG_DATA_SIZE              48    /**< Mifare Ultralight total number of data bytes on the tag. */

#define MUL_FIRST_PAGE                 0     /**< Mifare Ultralight first available page. */
#define MUL_LAST_PAGE                  15    /**< Mifare Ultralight last available page. */
#define MUL_NUM_PAGES                  16    /**< Mifare Ultralight total number of available pages. */

#define MUL_FIRST_DATA_PAGE            4     /**< Mifare Ultralight fisrt available data page. */
#define MUL_LAST_DATA_PAGE             15    /**< Mifare Ultralight last available data page. */
#define MUL_NUM_DATA_PAGES             12    /**< Mifare Ultralight total number of available data pages. */

#define MUL_LOCK_0_1_PAGE              2     /**< Mifare Ultralight Lock0 and Lock1 page. */

#define MUL_OTP_PAGE                   3     /**< Mifare Ultralight One Time Programmable page. */

class wMifareUL;

//! Mifare Ultralight MF0ICU1.
/**
 * This class is designed to read and write to Mifare Ultralight MF0ICU1 tags.
 * Care must be taken when writing data to these tags because writing data can
 * possibly cause the tag to be no longer accessible. It is important to read
 * the tag manufacturer documentation first to better understanding the
 * features and functions of the tag.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * Mifare Ultralight tags are divided into 16 pages with each page containing 4
 * bytes of data. 10 bytes are reserved for manufacturer data, 2 bytes are used
 * for the read-only locking mechanism, 4 bytes are available as One Time
 * Programmable (OTP) and 48 bytes are available as user programmable read / write
 * memory.\n
 * \n
 * Below is a diagram of the Mifare Ultralight (512 bit) EEPROM organized in 16
 * pages of 4 bytes each.\n
 * \n
 * <img src="MifareULMemory.jpg">
 * \n
 */
class MifareUL
{
public:

   /*! \cond */
   MifareUL();

   ~MifareUL();
   /*! \endcond */

   /**
    * Locates a Mifare Ultralight tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a Mifare Ultralight tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a Mifare Ultralight tag
    *            currently in the iCarte field. This buffer must have a minimum
    *            size of #MUL_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid);

   /**
    * Gets the Unique Identification (UID) number of the last Mifare Ultralight
    * tag that was located by a call to IsTagAvailable(). This method differs
    * from ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last Mifare Ultralight
    *            tag that was located using IsTagAvailable(). This buffer must
    *            have a minimum size of #MUL_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number of the Mifare Ultralight tag
    * currently in the iCarte field.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #MUL_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Reads data from the specified page of the Mifare Ultralight tag currently
    * in the iCarte field.
    *
    * \param page is the desired page to read from. Valid \a page values are from
    *             #MUL_FIRST_PAGE to #MUL_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #MUL_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPage(int page, unsigned char *readBuffer);

   /**
    * Reads 4 pages of data starting from the specified page of the Mifare
    * Ultralight tag currently in the iCarte field. A roll back is implemented;
    * e.g. if page 14 is specified, the contents of pages 14, 15, 0 and 1 are
    * returned.
    *
    * \param page is the desired starting page to read from. Valid \a page values
    *             are from #MUL_FIRST_PAGE to #MUL_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #MUL_PAGES_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPages(int page, unsigned char *readBuffer);

   /**
    * Writes data to the specified page of the Mifare Ultralight tag currently
    * in the iCarte field. Data can only be written to user pages that are not
    * locked.
    *
    * \param page is the desired page to write to. Valid \a page values are from
    *             #MUL_LOCK_0_1_PAGE to #MUL_LAST_PAGE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #MUL_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_MIFARE_UNKNOWN_NACK_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WritePage(int page, unsigned char *writeBuffer);

   /**
    * Writes data to the specified page of the Mifare Ultralight tag currently
    * in the iCarte field using the compatibility write command. Even though 16
    * bytes are transferred to the tag, only the least significant 4 bytes are
    * written to the specified page. It is recommended to set the remaining
    * 12 bytes to zero. Data can only be written to user pages that are not locked.
    *
    * \param page is the desired page to write to. Valid \a page values are from
    *             #MUL_LOCK_0_1_PAGE to #MUL_LAST_PAGE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #MUL_PAGES_SIZE.
    *                    Only the least significant 4 bytes are written to \a page
    *                    and it is recommended to set the remaining 12 bytes to zero.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_MIFARE_UNKNOWN_NACK_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WritePages(int page, unsigned char *writeBuffer);

private:

   /*! \cond */
   wMifareUL *wmifareul;
   /*! \endcond */
};

#endif
