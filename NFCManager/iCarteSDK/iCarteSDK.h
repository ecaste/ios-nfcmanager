/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        iCarteSDK.h
*
* Description: iCarte SDK header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __ICARTESDK_H__
#define __ICARTESDK_H__

#include "Errorcodes.h"
#include "Crosscheck.h"
#include "Utilities.h"
#include "SecureElement.h"
#include "ISO14443A_3.h"
#include "ISO14443A_4.h"
#include "ISO15693_3.h"
#include "Mifare1K.h"
#include "Mifare1KEmulator.h"
#include "Mifare4K.h"
#include "MifareUL.h"
#include "MifareULC.h"
#include "Kovio.h"
#include "Topaz.h"
#include "ICODESLI.h"

#endif
