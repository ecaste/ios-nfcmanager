/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        MifareULC.h
*
* Description: Mifare Ultralight C header file.
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __MIFAREULC_H__
#define __MIFAREULC_H__

#define MUC_UID_SIZE                   7     /**< Mifare Ultralight C Unique Identification (UID) number size in bytes. */

#define MUC_PAGE_SIZE                  4     /**< Mifare Ultralight C page size in bytes. */
#define MUC_PAGES_SIZE                 16    /**< Mifare Ultralight C pages size in bytes (4 pages). */

#define MUC_TAG_SIZE                   192   /**< Mifare Ultralight C total number of bytes (including UID, Internal, Locks, OTP and data) on the tag. */
#define MUC_TAG_DATA_SIZE              144   /**< Mifare Ultralight C total number of data bytes on the tag. */

#define MUC_FIRST_PAGE                 0     /**< Mifare Ultralight C first available page. */
#define MUC_LAST_PAGE                  43    /**< Mifare Ultralight C last available page. */
#define MUC_NUM_PAGES                  44    /**< Mifare Ultralight C total number of available pages. */

#define MUC_FIRST_DATA_PAGE            4     /**< Mifare Ultralight C fisrt available data page. */
#define MUC_LAST_DATA_PAGE             39    /**< Mifare Ultralight C last available data page. */
#define MUC_NUM_DATA_PAGES             36    /**< Mifare Ultralight C total number of available data pages. */

#define MUC_LOCK_0_1_PAGE              2     /**< Mifare Ultralight C Lock0 and Lock1 page. */
#define MUC_LOCK_2_3_PAGE              40    /**< Mifare Ultralight C Lock2 and Lock3 page. */

#define MUC_OTP_PAGE                   3     /**< Mifare Ultralight C One Time Programmable page. */

#define MUC_COUNTER_PAGE               41    /**< Mifare Ultralight C 16-bit counter page. */

#define MUC_FIRST_AUTH_CONFIG_PAGE     42    /**< Mifare Ultralight C first authentication configuration page. */
#define MUC_LAST_AUTH_CONFIG_PAGE      43    /**< Mifare Ultralight C last authentication configuration page. */

#define MUC_FIRST_AUTH_KEY_PAGE        44    /**< Mifare Ultralight C first authentication key page. */
#define MUC_LAST_AUTH_KEY_PAGE         47    /**< Mifare Ultralight C last authentication key page. */

class wMifareULC;

//! Mifare Ultralight C MF0ICU2.
/**
 * This class is designed to read and write to Mifare Ultralight C MF0ICU2 tags.
 * Care must be taken when writing data to these tags because writing data can
 * possibly cause the tag to be no longer accessible. It is important to read
 * the tag manufacturer documentation first to better understanding the
 * features and functions of the tag. Authentication is not currently supported.\n
 * \n
 * <b>Memory Organization</b>\n
 * \n
 * Mifare Ultralight C tags are divided into 48 pages with each page containing 4
 * bytes of data. 10 bytes are reserved for manufacturer data, 4 bytes are used
 * for the read-only locking mechanism, 4 bytes are available as One Time
 * Programmable (OTP), 2 bytes are available as a 16-bit counter and 144 bytes are
 * available as user programmable read / write memory.\n
 * \n
 * Below is a diagram of the Mifare Ultralight C (1536 bit) EEPROM organized in 48
 * pages of 4 bytes each.\n
 * \n
 * <img src="MifareULCMemory.jpg">
 * \n
 */
class MifareULC
{
public:

   /*! \cond */
   MifareULC();

   ~MifareULC();
   /*! \endcond */

   /**
    * Locates a Mifare Ultralight C tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates a Mifare Ultralight C tag in the iCarte field. This method must be
    * called first before calling any other methods in this class which access
    * the tag. This method should be used in a looping mechanism so that you can
    * detect when a tag is in range. For maximum speed make sure no other API
    * methods are called until a tag is found. Once a tag is available you may
    * start calling other methods in this class which access the tag.
    *
    * \param uid is a buffer used for storing the UID of a Mifare Ultralight C tag
    *            currently in the iCarte field. This buffer must have a minimum
    *            size of #MUC_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid);

   /**
    * Gets the Unique Identification (UID) number of the last Mifare Ultralight C
    * tag that was located by a call to IsTagAvailable(). This method differs
    * from ReadUID() because it doesn't read any data from the tag.
    *
    * \param uid is a buffer used for storing the UID of the last Mifare Ultralight
    *            C tag that was located using IsTagAvailable(). This buffer must
    *            have a minimum size of #MUC_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid);

   /**
    * Reads the Unique Identification (UID) number of the Mifare Ultralight C tag
    * currently in the iCarte field.
    *
    * \param uid is a buffer used for storing the UID that will be read from the
    *            tag currently in the iCarte field. This buffer must have a
    *            minimum size of #MUC_UID_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadUID(unsigned char *uid);

   /**
    * Reads data from the specified page of the Mifare Ultralight C tag currently
    * in the iCarte field.
    *
    * \param page is the desired page to read from. Valid \a page values are from
    *             #MUC_FIRST_PAGE to #MUC_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #MUC_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPage(int page, unsigned char *readBuffer);

   /**
    * Reads 4 pages of data starting from the specified page of the Mifare
    * Ultralight C tag currently in the iCarte field. A roll back is implemented;
    * e.g. if page 42 is specified, the contents of pages 42, 43, 0 and 1 are
    * returned.
    *
    * \param page is the desired starting page to read from. Valid \a page values
    *             are from #MUC_FIRST_PAGE to #MUC_LAST_PAGE.
    *
    * \param readBuffer is a buffer used for storing the data read from the tag.
    *                   This buffer must have a minimum size of #MUC_PAGES_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR ReadPages(int page, unsigned char *readBuffer);

   /**
    * Writes data to the specified page of the Mifare Ultralight C tag currently
    * in the iCarte field. Data can only be written to user pages that are not
    * locked.
    *
    * \param page is the desired page to write to. Valid \a page values are from
    *             #MUC_LOCK_0_1_PAGE to #MUC_LAST_PAGE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #MUC_PAGE_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_MIFARE_UNKNOWN_NACK_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WritePage(int page, unsigned char *writeBuffer);

   /**
    * Writes data to the specified page of the Mifare Ultralight C tag currently
    * in the iCarte field using the compatibility write command. Even though 16
    * bytes are transferred to the tag, only the least significant 4 bytes are
    * written to the specified page. It is recommended to set the remaining
    * 12 bytes to zero. Data can only be written to user pages that are not locked.
    *
    * \param page is the desired page to write to. Valid \a page values are from
    *             #MUC_LOCK_0_1_PAGE to #MUC_LAST_PAGE.
    *
    * \param writeBuffer is a buffer used for storing the data to write to the tag.
    *                    This buffer must have a minimum size of #MUC_PAGES_SIZE.
    *                    Only the least significant 4 bytes are written to \a page
    *                    and it is recommended to set the remaining 12 bytes to zero.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_GEN_NO_RESPONSE
    * \retval #ERR_MIFARE_UNKNOWN_NACK_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR WritePages(int page, unsigned char *writeBuffer);

private:

   /*! \cond */
   wMifareULC *wmifareulc;
   /*! \endcond */
};

#endif
