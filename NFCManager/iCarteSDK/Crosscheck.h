/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        Crosscheck.h
*
* Description: Crosscheck header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __CROSSCHECK_H__
#define __CROSSCHECK_H__

#define CROSSCHECK_UID_SIZE            10    /**< iCarte Unique Identification (UID) number size in bytes. */
#define CROSSCHECK_PASSWORD_SIZE       16    /**< iCarte password size in bytes. */
#define CROSSCHECK_CARRIER_NAME_SIZE   16    /**< iCarte Carrier Name size in bytes */
#define CROSSCHECK_APPLICATION_ID_SIZE 64    /**< iCarte Application ID size in bytes */
#define CROSSCHECK_USERNAME_SIZE       16    /**< iCarte Customer Username size in bytes */
#define CROSSCHECK_MINIMUM_SIZE        3     /**< Minimum iCarte Customer Username & Password size in bytes */

//! %Crosscheck Permanent Lockout Mode enumerator.
typedef enum CROSSCHECK_PERMANENT_LOCKOUT_MODE
{
   CROSSCHECK_PERMANENT_LOCKOUT_MODE_DISABLE  = 0x00,   /**< Disabled. */
   CROSSCHECK_PERMANENT_LOCKOUT_MODE_ENABLE   = 0x01    /**< Enabled. */
} CROSSCHECK_PERMANENT_LOCKOUT_MODE;

//! %Crosscheck Element State enumerator.
typedef enum CROSSCHECK_ELEMENT_STATE
{
   CROSSCHECK_ELEMENT_STATE_DISABLE          = 0x00,    /**< Disable. */
   CROSSCHECK_ELEMENT_STATE_ENABLE           = 0x01,    /**< Enable. */
   CROSSCHECK_ELEMENT_STATE_LEAVE_UNCHANGED  = 0x02,    /**< Leave Unchanged. */
   CROSSCHECK_ELEMENT_STATE_RESET            = 0x03,    /**< Reset. */
   CROSSCHECK_ELEMENT_STATE_NOT_APPLICABLE   = 0x10     /**< Not applicable. */
} CROSSCHECK_ELEMENT_STATE;

//! %Crosscheck Password Index enumerator.
typedef enum CROSSCHECK_PASSWORD_INDEX
{
   CROSSCHECK_PASSWORD_INDEX_1  = 0x81,      /**< Password 1. */
   CROSSCHECK_PASSWORD_INDEX_2  = 0x82,      /**< Password 2. */
   CROSSCHECK_PASSWORD_INDEX_3  = 0x83       /**< Password 3. */
} CROSSCHECK_PASSWORD_INDEX;

//! %Crosscheck Token enumerator.
typedef enum CROSSCHECK_TOKEN
{
   //! %Crosscheck Information Token: %Crosscheck Version.
   /**
    * This token is used for retrieving the %Crosscheck Version.
    *
    * Usage:
    * To retrieve the stored %Crosscheck Version.
    *   Get(CROSSCHECK_TOKEN_IDENTIFICATION_TYPE_CROSSCHECK_VERSION, value, length);
    * value will contain the stored numeric crosscheck version.
    * length will contain the size of the value.
    *
    */
   CROSSCHECK_TOKEN_IDENTIFICATION_TYPE_CROSSCHECK_VERSION                                      = 0x01,  /**< %Crosscheck Version Number Token. */

   //! %Crosscheck Information Token: Customer Service Phone Number.
   /**
    * This token is used for storing/retrieving the customer service phone number.
    *
    * Usage:
    * To retrieve the stored Customer Service Phone Number.
    *   Get(CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER, value, length);
    * value will contain the stored numeric customer service phone number.
    * length will contain the size of the value.
    *
    * To store the Customer Service Phone Number.
    *   Set(CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER, value, length);
    * value will contain the numeric customer service phone number to be stored.
    * length will contain the size of the value.
    */
   CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER                              = 0x15,  /**< Customer Service Phone Number Token. */

   //! %Crosscheck Security Token: Secure Element UID.
   /**
    * This token is used for enabling/disabling pre-security check of the Secure Element UID.
    *
    * Usage:
    * To retrieve the state of the Secure Element UID pre-security check.
    *   Get(CROSSCHECK_TOKEN_SECURITY_TYPE_SE_UID, state);
    * state will contain the stored value of the Secure Element UID pre-security check. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will contain the stored UID.
    * length will contain the size of the value.
    *
    * To store the state of the Secure Element UID pre-security check.
    *   Set(CROSSCHECK_TOKEN_SECURITY_TYPE_SE_UID, state);
    * state will contain the element state to be store. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will be ignored, the value should be set to NULL.
    * length will be ignored, the length should be set to 0.
    */
   CROSSCHECK_TOKEN_SECURITY_TYPE_SE_UID                                                        = 0x20,  /**<Smart Element UID Token. */

   //! %Crosscheck Security Token: UUID.
   /**
    * This token is used for enabling/disabling pre-security check of the UUID. The UUID is application specific and is
    * stored in the iPhones keychain. The keychain is persistent even if the application is removed.
    *
    * Usage:
    * To retrieve the state of the UUID pre-security check.
    *   Get(CROSSCHECK_TOKEN_SECURITY_TYPE_UUID, state);
    * state will contain the stored value of the UUID pre-security check. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will contain the stored UUID.
    * length will contain the size of the value.
    *
    * To store the state of the UUID pre-security check.
    *   Set(CROSSCHECK_TOKEN_SECURITY_TYPE_UUID, state);
    * state will contain the element state to be store. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will be ignored, the SDK obtains the UUID automatically from the iPhone. The value should be set to NULL.
    * length will be ignored, the SDK obtains the size of the value from the iPhone. The length should be set to 0
    */
   CROSSCHECK_TOKEN_SECURITY_TYPE_UUID                                                          = 0x21,  /**< Application UUID Token. */

   //! %Crosscheck Security Token: Carrier Name.
   /**
    * This token is used for enabling/disabling pre-security check of the Carrier Name.
    *
    * Usage:
    * To retrieve the state of the Carrier Name pre-security check and/or the value of the stored Carrier Name.
    *   Get(CROSSCHECK_TOKEN_SECURITY_TYPE_CARRIER_NAME, state, value, length);
    * state will contain the stored value of the Carrier Name pre-security check. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will contain the stored Carrier Name. (OPTIONAL)
    * length will contain the size of the value. (OPTIONAL)
    *
    * To store the state of the Carrier Name pre-security check and/or value of the Carrier Name.
    *   Set(CROSSCHECK_TOKEN_SECURITY_TYPE_CARRIER_NAME, state, value, length);
    * state will contain the element state to be store. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_LEAVE_UNCHANGED
    * value will be ignored, the SDK obtains the Carrier Name from the currently installed SIM. The value should be set to NULL.
    * length will be ignored, the SDK obtains the size of the value from the iPhone. The length should be set to 0
    */
   CROSSCHECK_TOKEN_SECURITY_TYPE_CARRIER_NAME                                                  = 0x23,  /**< Carrier Name Token. */

   //! %Crosscheck Security Token: RFID, Secure Element, %Utilities Access Password.
   /**
    * This token is used for enabling/disabling security check and storing the password index for the RFID, Secure Element, %Utilities Access Password.
    *
    * Usage:
    * To retrieve the state of the RFID, Secure Element, %Utilities Access Password security check and/or the value of the stored RFID, Secure Element, %Utilities
    * Access Password.
    *   Get(token, state, value, length);
    * token will contain the crosscheck element token. Possible values will include:
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_RFID_ACCESS_PASSWORD
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_ACCESS_PASSWORD
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UTILITIES_ACCESS_PASSWORD
    * state will contain the stored value of the security check for the respective token. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    * value will contain the stored password index for the respective token. Possible values include: (OPTIONAL)
    *   \li #CROSSCHECK_PASSWORD_INDEX_1
    *   \li #CROSSCHECK_PASSWORD_INDEX_2
    *   \li #CROSSCHECK_PASSWORD_INDEX_3
    * length will contain the size of the value. (OPTIONAL)
    *
    * To store the state of the Carrier Name pre-security check and/or value of the Carrier Name.
    *   Set(token, state, value, length);
    * token will contain the crosscheck element token. Possible values will include:
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_RFID_ACCESS_PASSWORD
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_ACCESS_PASSWORD
    *   \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UTILITIES_ACCESS_PASSWORD
    * state will contain the element state to be store. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_LEAVE_UNCHANGED
    * value will contain the stored password index for the respective token. (OPTIONAL)
    *   \li #CROSSCHECK_PASSWORD_INDEX_1
    *   \li #CROSSCHECK_PASSWORD_INDEX_2
    *   \li #CROSSCHECK_PASSWORD_INDEX_3
    * length will contain the size of the value.  (OPTIONAL)
    */
   CROSSCHECK_TOKEN_SECURITY_TYPE_RFID_ACCESS_PASSWORD                                          = 0x2A,  /**< RFID Access Password Token. */
   CROSSCHECK_TOKEN_SECURITY_TYPE_SE_ACCESS_PASSWORD                                            = 0x2B,  /**< Secure Element Access Password Token. */
   CROSSCHECK_TOKEN_SECURITY_TYPE_UTILITIES_ACCESS_PASSWORD                                     = 0x2C,  /**< %Utilities Access Password Token. */


   //! %Crosscheck Security Token: Permanent Lockout.
   /**
    * This token is used for enabling/disabling the Permanent Lockout.
    *
    * Usage:
    * To retrieve the state of the Permanent Lockout.
    *   Get(CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT, state);
    * state will contain the stored value of the Permanent Lockout. Possible values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    *
    * To store the state of the Permanent Lockout.
    *   Set(CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT, state);
    * state will contain the element state to be store. To reset a Permanent Locked iCarte, need to issue #CROSSCHECK_ELEMENT_STATE_RESET. Possible
    * values will include:
    *   \li #CROSSCHECK_ELEMENT_STATE_ENABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_DISABLE
    *   \li #CROSSCHECK_ELEMENT_STATE_RESET
    * length should be set to 1
    */
   CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT                                             = 0x2F,  /**< Permanent lockout Feature Token. */

   //! %Crosscheck Access Token: Customer Password 1/2/3.
   /**
    * This token is used for storing/retrieving the respective crosscheck elements.
    *
    * Usage:
    *
    * To store the value of the respective crosscheck element.
    *   Set(token, value, length);
    * token will contain the crosscheck element token. Possible values will include:
    *   \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1
    *   \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2
    *   \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3
    * value will contain the Customer Password to be stored for the respective crosscheck element. The buffer must not exceed #CROSSCHECK_PASSWORD_SIZE.
    * length will contain the size of the value. The length must not exceed #CROSSCHECK_PASSWORD_SIZE but must be at least greater than #CROSSCHECK_MINIMUM_SIZE.
    */
   CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1                                             = 0xE1,  /**< Customer 1 Password Token. */
   CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2                                             = 0xE2,  /**< Customer 2 Password Token. */
   CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3                                             = 0xE3,  /**< Customer 3 Password Token. */

   //! %Crosscheck Access Token: Customer Username.
   /**
    * This token is used for storing/retrieving the Customer Username.
    *
    * Usage:
    * To retrieve the value of the stored customer username.
    *   Get(CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME, value, length);
    * value will contain the stored Customer username.
    * length will contain the size of the value.
    *
    * To store the value of the customer username.
    *   Set(CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME, value, length);
    * value will contain the Customer username. The buffer must not exceed #CROSSCHECK_USERNAME_SIZE.
    * length will contain the size of the value. The length must not exceed #CROSSCHECK_USERNAME_SIZE but must be at least greater than #CROSSCHECK_MINIMUM_SIZE.
    */
   CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME                                               = 0xEA,  /**< Customer Username Token. */

} CROSSCHECK_TOKEN;

class wCrosscheck;

//! %Crosscheck
/**
 * This class provides utility functions required to obtain Security
 * configuration regarding the iCarte.
 */
class Crosscheck
{
public:

   /*! \cond */
   Crosscheck();

   ~Crosscheck();
   /*! \endcond */

   /**
    * Authenticates with the iCarte to be able to access %Crosscheck Security
    * Configuration. All methods within this class are protected until
    * developer successfully authenticates.
    *
    * \param password is a buffer used for storing the authentication password.
    *                This buffer must have a minimum size of
    *                #CROSSCHECK_MINIMUM_SIZE.
    *
    * \param passwordLength will contain the size of the value.
    *                The length must not exceed #CROSSCHECK_PASSWORD_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_INCORRECT_PASSWORD
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Authenticate(uint8_t *password, int passwordLength);

   /**
    * Changes the Authentication Password to obtain access to the %Crosscheck Security
    * Configuration. This method requires prior authentication before being used.
    *
    * \param newPassword is a buffer used for storing the authentication password.
    *                This buffer must have a minimum size of
    *                #CROSSCHECK_MINIMUM_SIZE.
    *
    * \param passwordLength will contain the size of the value.
    *                The length must not exceed #CROSSCHECK_PASSWORD_SIZE.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_NOT_AUTHENTICATED
    *
    * \see errorcodes.h for more detail on the meanings of the different error codes.
    */
   ERR ChangeAuthenticationPassword(uint8_t *newPassword, int passwordLength);

   /**
    * Gets the state and/or value of %Crosscheck element indicated by CROSSCHECK_TOKEN. This method requires prior
    * authentication before being used.  Each token has specific restrictions, please refer to further documentation for exact usage.
    *
    * \param token is a reference indicator to retrieve information of a crosscheck element.
    *              Tokens are of type #CROSSCHECK_TOKEN:
    *  \li #CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER references a Customer Service Phone Number.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_UID references the Secure Element UID.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UUID references the iPhone UUID.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_CARRIER_NAME  references the Carrier Name.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_RFID_ACCESS_PASSWORD references the RFID Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_ACCESS_PASSWORD references the Secure Element Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UTILITIES_ACCESS_PASSWORD references the %Utilities Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT references the Permanent Lockout Enable.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1 references the Customer Password 1.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2 references the Customer Password 2.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3 references the Customer Password 3.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME references the Customer username.
    *
    * \param state is an enumerated type indicating the state of crosscheck element.
    *              State is of type #CROSSCHECK_ELEMENT_STATE.  Valid states returned are:
    *  \li #CROSSCHECK_ELEMENT_STATE_DISABLE disables the element.
    *  \li #CROSSCHECK_ELEMENT_STATE_ENABLE enables the element.
    *  \li #CROSSCHECK_ELEMENT_STATE_NOT_APPLICABLE is not applicable to certain elements.
    *
    * \param value is a buffer used for storing the values for the crosscheck element. (optional)
    *
    * \param length for input the length of \a value, for output the number
    *                  of value bytes. (optional)
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_NOT_AUTHENTICATED
    * \retval #ERR_CROSSCHECK_INCORRECT_ARGUMENT
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Get(CROSSCHECK_TOKEN token, CROSSCHECK_ELEMENT_STATE *state, uint8_t *value, int *length);

   /**
    * Gets the state and/or value of %Crosscheck element indicated by CROSSCHECK_TOKEN. This method requires prior
    * authentication before being used.  Each token has specific restrictions, please refer to further documentation for exact usage.
    *
    * \param token is a reference indicator to retrieve information of a crosscheck element.
    *              Tokens are of type #CROSSCHECK_TOKEN:
    *  \li #CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER references a Customer Service Phone Number.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1 references the Customer Password 1.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2 references the Customer Password 2.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3 references the Customer Password 3.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME references the Customer username.
    *
    * \param value is a buffer used for storing the values for the crosscheck element.
    *
    * \param length for input the length of \a value, for output the number
    *                  of value bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_NOT_AUTHENTICATED
    * \retval #ERR_CROSSCHECK_INCORRECT_ARGUMENT
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Get(CROSSCHECK_TOKEN token, uint8_t *value, int *length);

   /**
    * Sets the state and/or value of a %Crosscheck element indicated by CROSSCHECK_TOKEN. This method requires prior
    * authentication before being used.  Each token has specific restrictions, please refer to further documentation for exact usage.
    *
    * \param token is a reference indicator to retrieve information of a crosscheck element.
    *              Tokens are of type #CROSSCHECK_TOKEN:
    *  \li #CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER references a Customer Service Phone Number.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_UID references the Secure Element UID.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UUID references the iPhone UUID.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_CARRIER_NAME  references the Carrier Name.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_RFID_ACCESS_PASSWORD references the RFID Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_SE_ACCESS_PASSWORD references the Secure Element Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_UTILITIES_ACCESS_PASSWORD references the %Utilities Access Password Index.
    *  \li #CROSSCHECK_TOKEN_SECURITY_TYPE_PERMANENT_LOCKOUT references the Permanent Lockout Enable.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1 references the Customer Password 1.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2 references the Customer Password 2.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3 references the Customer Password 3.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME references the Customer username.
    *
    * \param state is an enumerated type to optionally change the state of crosscheck element.
    *              State are of type #CROSSCHECK_ELEMENT_STATE:
    *  \li #CROSSCHECK_ELEMENT_STATE_DISABLE disables the element.
    *  \li #CROSSCHECK_ELEMENT_STATE_ENABLE enables the element.
    *  \li #CROSSCHECK_ELEMENT_STATE_LEAVE_UNCHANGED leaves the state of the element unchanged.
    *  \li #CROSSCHECK_ELEMENT_STATE_RESET resets the permanent locked iCarte.
    *  \li #CROSSCHECK_ELEMENT_STATE_NOT_APPLICABLE is not applicable to certain elements.
    *
    * \param value is a buffer used for storing the values for the crosscheck element. (optional)
    *
    * \param length for input the length of \a value, for output the number
    *                  of value bytes. (optional)
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_NOT_AUTHENTICATED
    * \retval #ERR_CROSSCHECK_INCORRECT_ARGUMENT
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Set(CROSSCHECK_TOKEN token, CROSSCHECK_ELEMENT_STATE state, uint8_t *value, int length);

   /**
    * Sets the state and/or value of a %Crosscheck element indicated by CROSSCHECK_TOKEN. This method requires prior
    * authentication before being used.  Each token has specific restrictions, please refer to further documentation for exact usage.
    *
    * \param token is a reference indicator to retrieve information of a crosscheck element.
    *              Tokens are of type #CROSSCHECK_TOKEN:
    *  \li #CROSSCHECK_TOKEN_INFORMATION_TYPE_CUSTOMER_SERVICE_PHONE_NUMBER references a Customer Service Phone Number.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_1 references the Customer Password 1.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_2 references the Customer Password 2.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_PASSWORD_3 references the Customer Password 3.
    *  \li #CROSSCHECK_TOKEN_ACCESS_TYPE_CUSTOMER_USERNAME references the Customer username.
    *
    * \param value is a buffer used for storing the values for the crosscheck element. (optional)
    *
    * \param length for input the length of \a value, for output the number
    *                  of value bytes. (optional)
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_CROSSCHECK_NOT_AUTHENTICATED
    * \retval #ERR_CROSSCHECK_INCORRECT_ARGUMENT
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Set(CROSSCHECK_TOKEN token, uint8_t *value, int length);

private:

   /*! \cond */
   wCrosscheck *wcrosscheck;
   /*! \endcond */
};

#endif
