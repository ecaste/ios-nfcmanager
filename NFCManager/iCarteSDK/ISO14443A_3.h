/*******************************************************************************
*
* Name:        iCarte SDK
*
* File:        ISO14443A_3.h
*
* Description: ISO/IEC 14443A-3 header file
*
* Version:     4.1.2
*
* RESTRICTED PROPRIETARY INFORMATION
*
* The information disclosed herein is the exclusive property of Wireless
* Dynamics Inc. and is not to be disclosed without the written consent of
* Wireless Dynamics Inc. No part of this publication may be reproduced or
* transmitted in any form or by any means including electronic storage,
* reproduction, execution or transmission without the prior written consent
* of Wireless Dynamics Inc.
*
* The user shall not, and shall not authorize another to, reverse engineer,
* decompile, disassemble, or perform any similar process on the application,
* libraries or any other software components unless such is permitted
* by Wireless Dynamics Inc.
*
* The recipient of this document, by its retention and use, agrees to
* respect the security status of the information contained herein.
*
* Copyright Wireless Dynamics Inc. (2012). Subject to change.
*
*******************************************************************************/
#ifndef __ISO14443A_3_H__
#define __ISO14443A_3_H__

#define ISO14443A_3_UID_SINGLE_SIZE    4     /**< ISO/IEC 14443A-3 Unique Identification (UID) number single size in bytes. */
#define ISO14443A_3_UID_DOUBLE_SIZE    7     /**< ISO/IEC 14443A-3 Unique Identification (UID) number double size in bytes. */
#define ISO14443A_3_UID_TRIPLE_SIZE    10    /**< ISO/IEC 14443A-3 Unique Identification (UID) number triple size in bytes. */

#define ISO14443A_3_ATQA_LENGTH        2     /**< ISO/IEC 14443A-3 ATQA size in bytes. */
#define ISO14443A_3_SAK_LENGTH         1     /**< ISO/IEC 14443A-3 SAK size in bytes. */

//! ISO 14443A-3 Tag Type enumerator.
typedef enum ISO14443A_3_TAG_TYPE
{
   ISO14443A_3_TAG_TYPE_ISO14443A  = 0x00,   /**< Default. Use this type if the tag you want to locate and communicate with is ISO/IEC 14443A-3 compliant. */
   ISO14443A_3_TAG_TYPE_M1K        = 0x10,   /**< Locate and communicate with a Mifare Classic 1K tag. */
   ISO14443A_3_TAG_TYPE_M4K        = 0x11,   /**< Locate and communicate with a Mifare Classic 4K tag. */
   ISO14443A_3_TAG_TYPE_MUL        = 0x12,   /**< Locate and communicate with a Mifare Ultralight tag. */
   ISO14443A_3_TAG_TYPE_TOPAZ      = 0x20,   /**< Locate and communicate with a Topaz tag. */
   ISO14443A_3_TAG_TYPE_M1KE       = 0x90    /**< Locate and communicate with the iCarte Mifare Classic 1K emulator. */
} ISO14443A_3_TAG_TYPE;

class wISO14443A_3;

//! ISO/IEC 14443-3 Type A Compliant Tags.
/**
 * This class is designed to locate ISO/IEC 14443A-3 compliant tags in the iCarte
 * field. This class will handle the tag initialization and anticollision only.
 * Use SendCommand() and GetResponse() to send proprietary protocols and commands
 * and get responses. ISO/IEC 14443A-4 transmission protocols and commands are not
 * handled by this class.\n
 * \n
 * Please refer to the ISO/IEC 14443-3 documentation for more information.\n
 * \n
 */
class ISO14443A_3
{
public:

   /*! \cond */
   ISO14443A_3();

   virtual ~ISO14443A_3();
   /*! \endcond */

   /**
    * Locates an ISO/IEC 14443A-3 compliant tag in the iCarte field. This method
    * must be called first before calling any other methods in this class which
    * access the tag. This method should be used in a looping mechanism so that
    * you can detect when a tag is in range. For maximum speed make sure no other
    * API methods are called until a tag is found. Once a tag is available you
    * may start calling other methods in this class which access the tag. After
    * a tag is located it is automatically selected so you do not need to call
    * Select(). See SetTagType() if you wish to locate non-compliant ISO/IEC
    * 14443A-3 tag. See #ISO14443A_3_TAG_TYPE for which non-compliant ISO/IEC
    * 14443A-3 tags are supported.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable();

   /**
    * Locates an ISO/IEC 14443A-3 compliant tag in the iCarte field. This method
    * must be called first before calling any other methods in this class which
    * access the tag. This method should be used in a looping mechanism so that
    * you can detect when a tag is in range. For maximum speed make sure no other
    * API methods are called until a tag is found. Once a tag is available you
    * may start calling other methods in this class which access the tag. After
    * a tag is located it is automatically selected so you do not need to call
    * Select(). See SetTagType() if you wish to locate non-compliant ISO/IEC
    * 14443A-3 tag. See #ISO14443A_3_TAG_TYPE for which non-compliant ISO/IEC
    * 14443A-3 tags are supported.
    *
    * \param uid is a buffer used for storing the UID of an ISO/IEC 14443A-3
    *            compliant tag currently in the iCarte field. This buffer must
    *            have a minimum size of #ISO14443A_3_UID_SINGLE_SIZE but should
    *            have a size of #ISO14443A_3_UID_TRIPLE_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_TAG_AVAILABLE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR IsTagAvailable(unsigned char *uid, int *uidLength);

   /**
    * Selects the specified tag for further communication.
    *
    * \param uid is the uid of the tag to select.
    * \param uidLength is the length of \a uid. The valid lengths are
    *                  ISO14443A_3_UID_SINGLE_SIZE, ISO14443A_3_UID_DOUBLE_SIZE,
    *                  or ISO14443A_3_UID_TRIPLE_SIZE;
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_OUT_OF_RANGE
    * \retval #ERR_ISO14443_COMMAND_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Select(unsigned char *uid, int uidLength);

   /**
    * Halts all active ISO/IEC 14443A-3 compliant tags in the iCarte field.
    *
    * \retval #ERR_NONE
    * \retval #ERR_ISO14443_NO_TAG_SELECTED
    * \retval #ERR_ISO14443_COMMAND_FAILED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR Halt();

   /**
    * Gets the Unique Identification (UID) number of the last ISO/IEC 14443A-3
    * compliant that was located by a call to IsTagAvailable().
    *
    * \param uid is a buffer used for storing the UID. This buffer must have a
    *            minimum size of #ISO14443A_3_UID_SINGLE_SIZE but should have a
    *            size of #ISO14443A_3_UID_TRIPLE_SIZE.
    *
    * \param uidLength for input the length of \a uid, for output the
    *                  number of UID bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_ISO14443_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetUID(unsigned char *uid, int *uidLength);

   /**
    * Gets the Answer To Request (ATQA) of the last ISO/IEC 14443A-3 compliant
    * tag that was located by a call to IsTagAvailable().
    *
    * \param atqa is a buffer used for storing the ATQA. This buffer must have a
    *             minimum size of #ISO14443A_3_ATQA_LENGTH. Please refer to the
    *             ISO/IEC 14443-3 specification for more information on how to
    *             decode this data.
    *
    * \param atqaLength for input the length of \a atqa, for output the number
    *                   of ATQA bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_ISO14443_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetATQA(unsigned char *atqa, int *atqaLength);

   /**
    * Gets the Select Acknowledge (SAK) of the last ISO/IEC 14443A-3 compliant
    * tag that was located by a call to IsTagAvailable().
    *
    * \param sak is a buffer used for storing the SAK. This buffer must have a
    *            minimum size of #ISO14443A_3_SAK_LENGTH. Please refer to the
    *            ISO/IEC 14443-3 specification for more information on how to
    *            decode this data.
    *
    * \param sakLength for input the length of \a sak, for output the
    *                  number of SAK bytes.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_ISO14443_NO_TAG_SELECTED
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetSAK(unsigned char *sak, int *sakLength);

   /**
    * Sets the ISO/IEC 14443A-3 tag type. Use this method to specify which ISO/IEC
    * 14443A-3 tags to locate and communicate with. By default the class tries
    * to locate and communicate with compliant ISO/IEC 14443A-3 tags (i.e.
    * #ISO14443A_3_TAG_TYPE_ISO14443A). If you wish to locate and communicate
    * with non-compliant ISO/IEC 14443A-3 tags (i.e. proprietary anticollision,
    * frames and protocols) set the tag type accordingly. Only non-compliant
    * ISO/IEC 14443A-3 tags listed are supported.
    *
    * \param tagType is the new ISO/IEC 14443A-3 tag type. See #ISO14443A_3_TAG_TYPE
    *                for more information.
    *
    * \retval #ERR_NONE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SetTagType(ISO14443A_3_TAG_TYPE tagType);

   /**
    * Gets the current ISO/IEC 14443A-3 tag type.
    *
    * \param tagType it the reference used for storing the current ISO/IEC 14443A-3
    *                tag type. See #ISO14443A_3_TAG_TYPE for more information.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetTagType(ISO14443A_3_TAG_TYPE &tagType);

   /**
    * Sends a command (transparent or proprietary data) to the last ISO/IEC 14443A-3
    * compliant tag located by a call to IsTagAvailable() or selected by a call
    * to Select(). The \a writeBuffer need not contain the SOF, CRC_A (i.e. CRC1
    * and CRC2) or EOF bytes.
    *
    * \param writeBuffer is a buffer used for storing the data to send to the
    *                    tag.
    *
    * \param writeLength is the number of bytes to send to the tag.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR SendCommand(unsigned char *writeBuffer, int writeLength);

   /**
    * Gets a response from the last ISO/IEC 14443A-3 compliant tag located by a
    * call to IsTagAvailable() or selected by a call to Select(). The \a readBuffer
    * contains the actual received data from the tag with the SOF, CRC_A (i.e.
    * CRC1 and CRC2) and EOF bytes removed.
    *
    * \param readBuffer is a buffer used for storing the response. This buffer
    *                   must be at least \a readLength bytes in length.
    *
    * \param readLength for input the length of \a readBuffer, for output the
    *                   number of bytes received.
    *
    * \retval #ERR_NONE
    * \retval #ERR_GEN_NULL_POINTER
    * \retval #ERR_GEN_BUFFER_OVERRUN
    * \retval #ERR_GEN_NO_RESPONSE
    *
    * \see Errorcodes.h for more detail on the meaning of different error codes.
    */
   ERR GetResponse(unsigned char *readBuffer, int *readLength);

private:

   /*! \cond */
   wISO14443A_3 *wiso14443A_3;
   /*! \endcond */
};

#endif
