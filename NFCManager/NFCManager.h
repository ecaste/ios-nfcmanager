//
//  NFCManager.h
//  NFCManager
//
//  Created by Emmanuel Castellani on 03/10/12.
//  Copyright (c) 2012 Emmanuel Castellani. All rights reserved.
//

#import <Foundation/Foundation.h>

// Notification names:
/**
 The name of the notification that is called when the NFC Device found a sync tag.
 */
extern NSString *const NOTIFICATION_NFC_DETECTED_FOUND_TAG;
/**
 The name of the notification that is called when the NFC Device found an unknown tag.
 */
extern NSString *const NOTIFICATION_NFC_DETECTED_UNKNOWN_TAG;
/**
 The name of the notification that is called when no NFC Device is connected.
 */
extern NSString *const NOTIFICATION_NO_NFC_DEVICE;
/**
 The name of the notification that is called when the NFC Device got connected.
 */
extern NSString *const NOTIFICATION_NFC_DEVICE_CONNECTED;
/**
 The name of the notification that is called when the NFC Device got disconnected.
 */
extern NSString *const NOTIFICATION_NFC_DEVICE_DISCONNECTED;

/**
 The name of the key of userInfo that is used to store the ID of the Sync Tag.
 */
extern NSString *const KEY_SYNC_ID      ;
/**
 The name of the key of userInfo that is used to store the ID of the Product Tag.
 */
extern NSString *const KEY_TAG_ID      ;

/**
 The searched tag type
 */
typedef NS_OPTIONS(NSUInteger, TAG_TYPE)
{
    TAG_TYPE_ICODESLI       = 1 << 0,
    TAG_TYPE_KOVIO          = 1 << 1,
    TAG_TYPE_MIFARE_1K      = 1 << 2,
    TAG_TYPE_MIFARE_4K      = 1 << 3,
    TAG_TYPE_MIFARE_UL      = 1 << 4,
    TAG_TYPE_MIFARE_ULC     = 1 << 5,
    TAG_TYPE_TOPAZ          = 1 << 6
};
/**
 Simple Class to manage NFC Actions.
 */
@interface NFCManager : NSObject

/**
 Creates and returns an instance of TNGNFCManager.
 @returns A newly initialized TNGNFCManager
 */
+ (NFCManager*)sharedNFCManager;
/**
 Starts NFC Read Action
 */
- (void) startNFC;
/**
 Stops the NFC Read Action and destroyes NFC Tag manager.
 */
- (void) stopNFC;
/**
 Return YES if a device is connected, otherwise NO
 */
- (BOOL) hasNFCDevice;
/**
 The frequency of reading for tagsin seconds
 Default : 0.7
 */
@property(nonatomic,assign)int SCAN_INTERVAL;
/**
 The pause in seconds before starting to read new tags
 Default : 3
 */
@property(nonatomic,assign)int SCAN_PAUSE;
/**
 The type of tag search :
 TAG_TYPE_ICODESLI,
 TAG_TYPE_KOVIO,
 TAG_TYPE_MIFARE_1K,
 TAG_TYPE_MIFARE_4K,
 TAG_TYPE_MIFARE_UL,
 TAG_TYPE_MIFARE_ULC,
 TAG_TYPE_MIFARE_TOPAZ
 */
@property(nonatomic)TAG_TYPE SCAN_TAG_TYPE;
/**
 If nil, all scan tags will be read
 If !=nil, only tags that contain SCAN_TAG_MASK will be read
*/
@property(nonatomic,strong)NSString *SCAN_TAG_MASK;
@end
